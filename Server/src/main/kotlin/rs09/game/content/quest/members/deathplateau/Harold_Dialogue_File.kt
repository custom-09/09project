package rs09.game.content.quest.members.deathplateau

import api.*
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.state.EntityState
import core.game.world.update.flag.context.Animation
import org.rs09.consts.Animations
import org.rs09.consts.Items
import org.rs09.consts.NPCs
import rs09.game.content.dialogue.DialogueFile
import rs09.tools.END_DIALOGUE
import rs09.tools.secondsToTicks

/**
 * @author qmqz
 */


class Harold_Dialogue_File(val it: Int) : DialogueFile() {

    override fun handle(interfaceId: Int, buttonId: Int) {
        npc = NPC(NPCs.HAROLD_1078)
        val f = FacialExpression.FRIENDLY
        val ale = Items.ASGARNIAN_ALE_1905
        //val blur = Items.BLURBERRY_SPECIAL_2064
        val blurpre = Items.PREMADE_BLURB_SP_2028

        val blur = intArrayOf(Items.BLURBERRY_SPECIAL_2064, Items.PREMADE_BLURB_SP_2028)

        val q = "Death Plateau"
        var betAmount = 0
        var haroldMoney = 200

        when (it) {

            0 -> {
                when (stage) {
                    0 -> player(f, "Hello there.").also { stage++ }
                    1 -> npc(f, "Hi.").also { stage++ }
                    2 -> player(f, "Can I buy you a drink?").also { stage++ }
                    3 -> npc(FacialExpression.HAPPY, "Now you're talking! An Asgarnian Ale, please!").also { stage++ }

                    4 -> {
                        if (inInventory(player!!, ale, 1)) {
                            removeItem(player!!, ale)
                            sendMessage(player!!, "You give Harold an Asgarnian Ale.")
                            sendItemDialogue(player!!, ale, "You give Harold an Asgarnian Ale.").also { stage++ }
                            when (questStage(player!!, q)) {
                                2 -> {
                                    setQuestStage(player!!, q, 3)
                                    setAttribute(player!!, "/save:quest-death-plateau-alegiven", true)
                                }
                            }
                        } else {
                            player(FacialExpression.FRIENDLY, "I'll go and get you one.").also { stage = END_DIALOGUE }
                        }
                    }

                    5 -> {
                        end()
                        //WOT DE FUK MEN?? ANIM DON'T WERKKKK BIIIITCCCCCCHHHHHH
                        //BLOODY SON OF BITCH BLOODY
                        animate(npc!!, Animation(Animations.HUMAN_EATTING_829), true)
                        runTask(npc!!, 5) {
                            npc(FacialExpression.FRIENDLY, "*burp*").also { stage = END_DIALOGUE }
                        }
                    }
                }
            }

            1 -> {
                when (stage) {
                    0 -> player(f, "Hello there.").also { stage++ }
                    1 -> npc(f, "Hi.").also { stage++ }
                    2 -> options(
                        "Where were you when you last had the combination?",
                        "Would you like to gamble?",
                        "Can I buy you a drink?"
                    ).also { stage++ }
                    3 -> when (buttonId) {
                        1 -> player(
                            FacialExpression.ASKING,
                            "Where were you when you last had the combination?"
                        ).also { stage = 20 }
                        3 -> player(f, "Can I buy you a drink?").also { stage = 30 }
                    }

                    20 -> npcl(
                        FacialExpression.FRIENDLY,
                        "I honestly don't know! I've looked everywhere. I've searched the castle and my room!"
                    ).also { stage++ }
                    21 -> player(
                        FacialExpression.ASKING,
                        "Have you tried looking between here and the castle?"
                    ).also { stage++ }
                    22 -> npc(f, "Yeah, I tried that.").also { stage++ }
                    23 -> npc(f, "I need another beer.").also {
                        stage = END_DIALOGUE
                    }

                    30 -> if (getAttribute(player!!, "quest-death-plateau-alegiven", false)) {
                        npcl(f, "Sounds good! I normally drink Asgarnian Ale, but you know what?").also { stage++ }
                    }

                    31 -> player(FacialExpression.ASKING, "What?").also { stage++ }
                    32 -> npcl(
                        f,
                        "I really fancy one of those Blurberry Specials. I never get over to the Gnome Stronghold, so I haven't had one for ages!"
                    ).also { stage++ }
                    33 -> {
                        if (inInventory(player!!, blur[0]) || inInventory(player!!, blur[1])) {
                            removeItem(player!!, blur)
                            sendMessage(player!!, "You give Harold a Blurberry Special.")
                            sendItemDialogue(player!!, blur[0], "You give Harold a Blurberry Special.").also { stage++ }
                            setQuestStage(player!!, q, 4)

                        } else {
                            player(FacialExpression.FRIENDLY, "I'll go and get you one.").also { stage = END_DIALOGUE }
                        }
                    }

                    34 -> {
                        end()

                        //WOT DE FUK MEN?? ANIM DON'T WERKKKK BIIIITCCCCCCHHHHHH
                        //BLOODY SON OF BITCH BLOODY
                        animate(npc!!, Animation(Animations.HUMAN_EATTING_829), true)
                        npc!!.sendChat("Wow!")

                        runTask(npc!!, 3) {
                            //WHAT THE FUCK BLOODY NO STUN BLOODY
                            npc!!.stateManager.set(EntityState.STUNNED, secondsToTicks(3))
                            npc(FacialExpression.DRUNK, "Now THAT hit the spot!").also { stage = END_DIALOGUE }
                        }
                    }
                }
            }

            2 -> {
                when (stage) {
                    0 -> player(f, "Hello there.").also { stage++ }
                    1 -> npc(FacialExpression.DRUNK, "'Ello matey!'").also { stage++ }
                    2 -> options(
                        "Where were you when you last had the combination?",
                        "Would you like to gamble?",
                        "Can I buy you a drink?").also { stage++ }
                    3 -> when (buttonId) {
                        2 -> player(FacialExpression.ASKING, "Would you like to gamble?").also { stage = 10 }
                    }

                    10 -> npc(FacialExpression.DRUNK,"Shure!").also { stage++ }
                    11 -> npc(FacialExpression.DRUNK,"Place your betsh pleashe!").also { stage++ }
                    12 -> npc(FacialExpression.DRUNK,"*giggle*!").also { stage++ }
                    13 -> {
                        sendInputDialogue(player!!, true, "Enter amount:") { value ->
                            betAmount = value as Int }.also { stage++ }
                    }
                    14 -> npc(FacialExpression.DRUNK, "Right...er...here goes...").also { stage++ }
                    15 -> {
                        /*
                         *todo
                         * dice rolling interface
                         * gotta keep playing until you take the kike's 200gp
                         * if you win under his total amt, you get 2x the bet value,
                         * otherwise you get an additional 0.75x of the bet before the iou note
                         */

                        openInterface(player!!, 99)
                        setInterfaceText(player!!, player!!.name, 99, 6)
                    }
                    16 -> {
                        if (haroldMoney <= 0) {
                            npc(FacialExpression.DRUNK, "I sheemed to have rolled a one.").also { stage++ }
                            npc(FacialExpression.DRUNK, "Um...not enough money.").also { stage = 30 }
                        }
                    }
                    17 -> sendDialogue(player!!, "Harold is so drunk he can hardly see, let alone count!").also { stage++ }
                    18 -> {
                        sendItemDialogue(player!!, Items.COINS_995, "Harold has given you your winnings!")
                        sendMessage(player!!, "Harold has given you your winnings!")
                        sendMessage(player!!, "Harold is so drunk he can hardly see, let alone count!")
                        setQuestStage(player!!, "Death Plateau", 5)
                    }

                    30 -> npc(FacialExpression.DRUNK, "Heresh shome of it.").also { stage++ }
                    31 -> {
                        addItem(player!!, Items.COINS_995, betAmount * 0.75.toInt())
                        sendItemDialogue(player!!, Items.COINS_995, "Harold has given you some of your winnings!")
                    }
                    32 -> npc(FacialExpression.DRUNK, "I owe you the resht!").also { stage++ }
                    33 -> {
                        sendItemDialogue(player!!, Items.IOU_3103, "Harold has given you an IOU scribbled on some paper.")
                        sendMessage(player!!, "Harold has given you an IOU scribbled on some paper.")
                    }

                }

            }
            //addItemOrDrop(player!!, Items.IOU_3103)

            /*
             npc(FacialExpression.FRIENDLY, "").also { stage++ }
             player(FacialExpression.FRIENDLY, "").also { stage++ }

             npcl(FacialExpression.FRIENDLY, "").also { stage++ }
             playerl(FacialExpression.FRIENDLY, "").also { stage++ }
             */
        }
    }
}
