package rs09.game.content.dialogue.region.burthorpe

import api.openDialogue
import api.questStage
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.Items
import org.rs09.consts.NPCs
import rs09.game.content.quest.members.deathplateau.Harold_Dialogue_File

/**
 * @author qmqz
 */

@Initializable
class HaroldDialogue(player: Player? = null) : DialoguePlugin(player) {

    val f = FacialExpression.FRIENDLY
    val ale = Items.ASGARNIAN_ALE_1905

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        when (questStage(player!!, "Death Plateau")) {
            0, 1, 2 -> openDialogue(player, Harold_Dialogue_File(0))
            3 -> openDialogue(player, Harold_Dialogue_File(1))
            4 -> openDialogue(player, Harold_Dialogue_File(2))
            5 -> openDialogue(player, Harold_Dialogue_File(3))
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {

        }
        return true
    }

    /*
    npc(FacialExpression.FRIENDLY, "").also { stage++ }
    player(FacialExpression.FRIENDLY, "").also { stage++ }

    npcl(FacialExpression.FRIENDLY, "").also { stage++ }
    playerl(FacialExpression.FRIENDLY, "").also { stage++ }
    */

    override fun newInstance(player: Player?): DialoguePlugin {
        return HaroldDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.HAROLD_1078)
    }
}
