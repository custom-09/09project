package rs09.game.content.quest.members.tearsofguthix

import api.hasLevelStat
import core.game.node.entity.player.Player
import core.game.node.entity.player.link.quest.Quest
import core.game.node.entity.skill.Skills
import core.plugin.Initializable

/**
 * @author qmqz
 */

@Initializable
class TearsOfGuthix : Quest("Tears of Guthix",120, 119, 1, 449, 0, 1, 42) {

    override fun drawJournal(player: Player?, stage: Int) {
        super.drawJournal(player, stage)

        var line = 12
        var stage = getStage(player)

        when (stage) {
            0 -> {
                line(player, "I can start this quest by speaking to !!Juna the serpent?? who", line++)
                line(player, "lives deep in the !!Lumbridge Swamp Caves??.", line++)
                line(player, "I will need to have: ", line++)
                line(player, "Level 49 firemaking", line++, hasLevelStat(player!!, Skills.FIREMAKING, 49))
                line(player, "Level 20 crafting", line++, hasLevelStat(player, Skills.CRAFTING, 49))
                line(player, "Level 20 mining", line++, hasLevelStat(player, Skills.MINING, 49))
                line(player, "43 quest points", line++, questPoints >= 43)
                line(player, "!!Level 49 crafting would be an advantage??", line++, hasLevelStat(player, Skills.CRAFTING, 49))
                line(player, "!!Level 49 smithing would be an advantage??", line++, hasLevelStat(player, Skills.SMITHING, 49))
            }

            //line(player,"", line++)

        }
    }

    override fun finish(player: Player) {
        var ln = 10
        super.finish(player)
        player.packetDispatch.sendString("You have completed the $name Quest!", 277, 4)
        player.packetDispatch.sendItemZoomOnInterface(1891, 240, 277, 5)

        drawReward(player,"1 Quest Point", ln++)
        drawReward(player,"1,000 Crafting XP", ln++)
        drawReward(player,"Access to the Tears of Guthix", ln++)
        drawReward(player,"cave", ln++)

        player.skills.addExperience(Skills.CRAFTING, 1000.0)

    }

    override fun newInstance(`object`: Any?): Quest {
        return this
    }
}