package rs09.game.content.dialogue.region.rellekka.mountaincamp

import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

@Initializable
class JokulDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        npcl(FacialExpression.FRIENDLY,"You are unusually helpful for an outerlander, ${player.getAttribute("fremennikname","fremmyname")}.").also { stage = 0 }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> player(FacialExpression.NEUTRAL, "Ehm, thanks... I guess.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return JokulDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.JOKUL_1810)
    }

}