package rs09.game.content.dialogue.region.rellekka.mountaincamp

import api.isQuestComplete
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

@Initializable
class CampDwellerDialogue(player: Player? = null) : DialoguePlugin(player) {

    val f = FacialExpression.FRIENDLY

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        when ((1..5).random()) {
            1 -> player(f, "Nice day, isn't it?").also { stage = 0 }
            2 -> player(f, "Hello!").also { stage = 10 }
            3 -> player(f, "Nice place you have here!").also { stage = 20 }

            4 -> if (isQuestComplete(player, "Mountain Daughter")) {
                npc(f, "I've heard about all the things you did for us!").also { stage = 30 }
            }

            5 -> npc(FacialExpression.NEUTRAL, "I'd rather be left alone, outerlander.").also { stage = END_DIALOGUE }
        }

        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when(stage){
            0 -> npc(f, "Yes.").also { stage = END_DIALOGUE }
            10 -> npc(f, "Hello.").also { stage = END_DIALOGUE }
            20 -> npc(f, "It is indeed.").also { stage = END_DIALOGUE }

            30 -> player(f, "I was happy to help.").also { stage++ }
            31 -> npcl(f, "And I'm happy that you helped us! Maybe now this place can be a bit more cheerful again!").also { stage = END_DIALOGUE }

            99 -> end()
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return CampDwellerDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.CAMP_DWELLER_1814, NPCs.CAMP_DWELLER_1815, NPCs.CAMP_DWELLER_1816, NPCs.CAMP_DWELLER_1817, NPCs.CAMP_DWELLER_1818)
    }
}
