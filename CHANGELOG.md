"So be it! Let my life fuel the spell that ends his!"

 redwings Update Log  


The latest changes are at the top, and the less latest changes are at the bottom.

June 16 -> June 26 2022
Slayer Master: Sumona
- Sumona, an incarnation of the god Amascut, has now been placed in-game, and hooked up to the usual Slayer Master functions. Sumona requires at least 90 combat, and 35 Slayer to request tasks from. She comes complete with her Slayer task pool and correct weights. Players can find her in a house in Pollnivneach.

Slayer Task Pool Corrections
- Slayer task pools have been cleaned up and corrected for each Slayer Master, with correct weights. Additionally, a currently unused Slayer tasks has been added back to the game: Elves. They are assigned by Chaeldar, Sumona and Duradel.

Teleblock!
- Teleblock now correctly persists on logout
- Teleblock can now function correctly on artificial players

Green Dragon Bot Improvements
- GD bots have been cleaned up and improved. An important one to note is they are now able to be damaged by Revenants, as well as being able to get teleblocked from the above TB fixes.

GWD Minion Aggression
GWD boss minions weren't continuing to attack after a respawn cycle, and becoming tolerant. This fixes that.
- Disable aggression tolerance for GWD minion NPCs

Removal of the Management Server
The server involved with holding the worlds and worldlist is old, clunky, and causes more issues and lockouts than it solves problems. It has been disabled, and appropriate functionality has been built into the gameserver to operate most management-related functions (clan, worldlist, friends online/offline etc) on its own, and work standalone. This means those annoying moments where you are locked out after a crash or relog will be going away for good. As a tradeoff however, multiple worlds cannot be connected at this time. Multi-world support will return when a new Management Server is written.

World Preloading and Cached Clipping
World Preloading is a new, toggleable feature that will preload game regions into memory, reducing post-launch disk access and read-writes. This increases idle memory usage of the server by ~2GB, but increases performance immensely, making for much more responsive ticks and actions.

On top of this, projectile and region clipping flags have also been cached, so when they are needed to be looked up, they will be accessible faster.

- Toggleable World Preloading to reduce post-startup disk access and I/O operations

- Cached region clipping flags for faster lookups

- Minor optimizations to setAttribute codepath which avoids unnecessary checks


Combat Pathfinding Fixes
- Fixed a bug that would allow entities to attack through walls

- Fixed two separate bugs that would cause odd behavior attacking large entities from certain angles

- Fixed a bug where NPCs couldn't be attacked against walls


Misc Updates
- Martin Thwait's Lost and Found (Rogue's Den) is now correctly a High Alchemy General Store, buying your item initially at 60% value, similar to Bandit Duty Free in the Wilderness

- Fixes to Shield of Arrav so you can't unintentionally get stuck

- Trimmed all the fat out of the global.sql file

- Ensured that all but the members and worlds tables could be safely dropped out of the DB

- There is now better handling for creating the default server account and creating the default server clan (fully automatic)

- Fixed bugs related to interfaces flagged as uncloseable that would cause stack overflows

- Fixed both tutorial sequence breaks (ladder + early home tele)

- Fixed bug that prevented bots from taking damage

- Fixed a bug where players would not be in the default clan after completion of the tutorial

- Intentionally burning will now stop at any specified Make-X

- Correct seaweed/soda ash burning pulse continuing indefinitely



June 15 2022
Fremennik Easy Diary
- With the completion of The Fremennik Trials, the last roadblock to finishing the diary has been cleared. The Fremennik Easy Diary is now fully completable, thanks to most of the work being laid down by Phil, and polished off by Crash. There's a few bandaids in the mix due to unimplemented minigames, but now the Fremennik sea boots 1 are properly obtainable for players!

- This means after completion of TFT, players can now talk to Peer the Seer and use his Deposit box functionality. This is a great way to speed up gathering Snape grass!

Pathfinding Fixes
- Pathfinder now correctly paths to the closest accessible tile of an object, not the closest tile numerically. This should fix instances where players end up running around an object before interacting with it.

Falador Farm Sheepdog
- The Falador farm sheepdog can now be fed meat and bones, for some authentic interativity!

June 13 2022
Wilderness Revival Update/redwings Version 1.2

One of the biggest updates yet in redwings first six months! New cache additions from the sorceror have allowed some more new content to enter the game, new friends, new foes, and an overhauled Wilderness. Clutch your spades and rune scimitars tight tonight!

Revenant Rework:
- Revenants have been entirely reworked in logic, allowing for proper patrols, resetting, spawn zones, idle logic, and the ability to form a group and patrol as one unit. As such, to make sure there are appropriate amounts in each part of the heatmap, the current "default" maximum cap of Revenant spawns has now increased to 30. Wow, that's a lot! They now correctly follow patrol routes outlined in a Revenant heatmap from 2010, which is avaliable for perusal on request, and pinned on the appropriate knowledge channels.

- This in turn makes the Wilderness a lot more of a dangerous place, but potentially a more profitable one. Revenants roll the PVP and Brawler glove tables, as well as hosting a swanky selection of item drops themselves, ranging from common alchables, to uncharged Amulets of glory.

Revenant Drop Table Rework:
As we lack data almost entirely for what they officially dropped within the 08-09 time period (apart from coin drops, dragon daggers and uncharged Amulets of Glory rarely, from some clips and threads at the time), I'm tuning this weird table to ensure it isn't over-profitable.

Trying to make the item amounts less RSPS-like and more in the realm of risk-for-reward, with hotspots working making revenant hunting a now-viable option for moneymaking and combat.

- Bumped down number of adamantite ore dropped from 1-75 to 1-5, and reduced their drop rate so they are much rarer
- Adjusted number of Uncut diamond to maximum of 5 (down from 50)
- Changed Gold bar drop to ore drop, and reduced maximum amount to 10-30
- Adjusted strength potion drop to 1-4
- Adjusted grimy ranarr drop to 1-10 (from 1-15)
- Adjusted grimy toadflax drop to 1-10 (from 5-15)
- Adjusted amount of Law, Blood and Death runes dropped to 15-200 (from 50-500)
- Removed 1000-15000 spirit shards drop
- Removed noted 100-500 Iron Ore drop
- Removed noted 50-250 Coal drop
- Removed dragon platelegs and plateskirt drops
- Removed ring of wealth drop
- Removed dragon boots drop
- All amulet of glory drops are now uncharged amulet of glory
- Adjusted Yew, Magic, Snapdragon, Palm Tree and papaya seed to only drop a maximum of 1 seed at a time, and reduced their drop weight so they are much rarer
- Limited tuna potato drop to a maximum of 5
- Adjusted weight of Yew logs drop to be higher (filler drop)
- Runite bolts reduced to 50 (from 100)
- Adamant bolts reduced to 75 (from 300)
- Rune arrows reduced to 75 (from 150)
- Removed 10-500 snape grass drop

Crazy Archaeologist:
- Crazy Archaeologist has been added to the game, examining some ruins in the Wilderness. He is a Ranged and Melee-based attacker, with an attack speed of 3, and has a special attack that can toss explosive books in a 3x3 area of effect. He has a respawn rate of 50 ticks/30 seconds. He can be found examining the Western Ruins at level ~22 Wilderness, right between the spawn zone for Revenenants, as well as a Multi-combat zone. Risk and reward is the name of the game in the barren lands of the Wilderness.

- Unique drop: Rune whip ~1/128

Basilisk Jaw:
- Basilisk jaw enters the game, allowing players to combine it with Helm of Neitiznot to create a Neitiznot Faceguard

- Neitiznot faceguard given a Summoning defence bonus of +10, up from Helm of Neitiznot's +8, as the Faceguard was lacking one before (consistency)


Dagannoth Rex (DKS) Drop Changes:
- Basilisk jaw added to droptable, at ~1/1024
- Dragon axe, Warrior ring and Berserker ring droprate corrected to ~1/128


Dagannoth Prime (DKS) Drop Changes:
- Dragon axe, Mud battlestaff, and Seers ring droprate corrected to ~1/128

Dagannoth Supreme (DKS) Drop Changes:
- Dragon axe, Seercull, and Archers ring droprate corrected to ~1/128

Brine Rat Drop Changes
- Brine sabre droprate corrected to ~1/512


Staff of Light:
- Staff of Light enters the game with correct special attack, and handling for its 15% Magic str bonus, as well as rune negation chance. Players can obtain it from defeating K'ril Tsutsaroth, leader of Zamorak's army in the God Wars Dungeon.


GWD Drop Changes:
A huge cleanout of GWD Boss tables, re-weighting the uniques to where they should be, as well as some other tertiary drops such as Long bones and Clue scrolls.

K'ril Tsutsaroth (Zamorak GWD) Drop Changes:
- Staff of Light now obtainable from K'ril Tsutsaroth at ~1/512 drop
- Zamorakian spear droprate corrected to ~1/512
- Steam battlestaff droprate corrected to ~1/512
- Godsword shard 1, 2 and 3 droprate corrected to ~1/768
- Dragon dagger (p++) droprate corrected to ~1/63
- Clue scroll (hard) droprate corrected to ~1/128
- RDT roll corrected to ~1/128
- Erroneous Nothing drop removed

General Graardor (Bandos GWD) Drop Changes:
- Bandos tassets, chestplate, and boots droprate corrected to ~1/381
- Bandos hilt droprate corrected to ~1/512
- Godsword shard 1, 2 and 3 droprate corrected to ~1/768
- Long bone droprate corrected to ~1/400
- Curved bone droprate corrected to ~1/5000
- Clue scroll (hard) droprate corrected to ~1/128
- RDT roll corrected to ~1/128
- Erroneous Nothing drop removed

Kree'arra (Armadyl GWD) Drop Changes:
- Armadyl helmet, chestplate, and plateskirt droprate corrected to ~1/381
- Armadyl hilt droprate corrected to ~1/512
- Godsword shard 1, 2 and 3 droprate corrected to ~1/768
- Long bone droprate corrected to ~1/400
- Curved bone droprate corrected to ~1/5000
- Clue scroll (hard) droprate corrected to ~1/128
- RDT roll corrected to ~1/128
- Erroneous Nothing drop removed
- Erroneous 1-20 Shark drop removed

Zilyana (Saradomin GWD) Drop Changes:
- Saradomin hilt droprate corrected to ~1/512
- Saradomin sword droprate corrected to ~1/128
- Godsword shard 1, 2 and 3 droprate corrected to ~1/768
- Clue scroll (hard) droprate corrected to ~1/128
- RDT roll corrected to ~1/128
- Erroneous Nothing drop removed

GWD Boss Pets:
- A global boss pet drop system has been implemented, and you can now obtain a pet of each specific God Wars General at a ~1/5000 chance.
- Pet k'ril tsutsaroth, pet kree'arra, pet zilyana and pet general graardor have been added to the game cache

Jad Pet:
- You now have a chance to obtain a TzRek-Jad pet by defeating Jad in the Fight Caves, at a ~1/200 chance
- You can now sacrifice Fire Capes at a chance for a pet TzRek-Jad (~1/200 chance)
- Combining these two pet obtain methods together is essentially forcing the rate to be doubled
- TzRek-Jad has been added to the game cache

- All newly added pets in the 1.2 Update now have appropriate dialogue transcribed from OSRS

Dragon Flail - Special Attack: Thrash
- The Dragon flail has entered the game, obtainable as a ~1/768 chance reward from a successful Barrows run.

- Special Attack: Thrash
- Thrash costs 50% special attack energy, spinning the flail and hitting the target. If in a multi-combat area, hit all entities around the player.

- This new dragon weapon will open up a bit of extra EXP gaining on multi-combat training methods, without really introducing any new power. The Crush archetype got some much needed love this update, which leads to the following:

Melee Arsenal Expansion:
A large change to an existing weapon type, two new weapon types, and a final piece in a set.

Warhammers:
- Warhammers now require Strength to equip, instead of Attack. This means that the Strength Cape bonus can now affect them, and they should scale more off of Strength at higher bonuses. All warhammers including Statius' Warhammer are affected by this change, and will require Strength to equip in the next update.

This change also gives a dedicated weapon for Strength-based accounts to train with, as well as opening up further avenues for Strength-based pure account builds.

- Dragon warhammer has been added to the game cache, completing the arsenal of warhammers. It is currently undecided what to do with this weapon, so it will remain unobtainable at this current stage.

Whips:
- Whips from bronze-rune have been added to the game cache

- Whips are 1H Slash-based ATK weapons with an inability to use Strength-based attack styles. They have an attack tickrate of 4, on-par with the scimitar and Abyssal whip, but have slightly lower Slash bonus, with slightly higher Strength bonus.

- Currently, only the rune whip is obtainable as a unique drop, the rest are found in the pockets of various monsters across the FellerScape

Flails:
- Flails from bronze-dragon have been added to the  game cache

- Flails are 2h Crush-based ATK weapons, with an attack tickrate of 5. They have stats similar to maces, with slightly higher Stab bonus, and a much higher Crush and Strength bonus due to being 2-handed.

- Currently, only the Dragon flail is obtainable as a unique drop from Barrows. You can find most common metal flails at Flynn's Mace Market in Falador, as well as on various monster drop tables

Stats Page Update:
- ::stats now shows number of Venenatis and Crazy Archaeologist kills, as well as how many Dragon Pickaxes,Treasonous Rings and Basilisk jaws the specific player has obtained

Minor changes
- Kandarin coal truck shooting star location moved slightly so it isn't stuck inside of objects



Cache changes
Wilderness Revival/redwings Version 1.2 Cache:

New Item IDs:
TzRek-Jad - 14736
Pet zilyana - 14737
Pet general graardor - 14738
Pet k'ril tsutsaroth - 14739
Pet kree'arra - 14740
Dragon warhammer - 14741
Dragon warhammer (noted) - 14742
Yellow parasol - 14743
Blue parasol - 14744
Green parasol - 14745
Purple parasol - 14746
White parasol - 14747
Red parasol - 14748
Basilisk jaw -           14749
Basilisk jaw (noted) -   14750
Staff of light -         14751
Staff of light (noted) - 14752
Bronze whip              14753
Bronze whip (noted)      14754
Iron whip                14755
Iron whip (noted)        14756
Steel whip               14757
Steel whip (noted)       14758
Black whip               14759
Black whip (noted)       14760
White whip               14761
White whip (noted)       14762
Mithril whip             14763
Mithril whip (noted)     14764
Adamant whip             14765
Adamant whip (noted)     14766
Rune whip                14767
Rune whip (noted)        14768
Bronze flail             14769
Bronze flail (noted)     14770
Iron flail               14771
Iron flail (noted)       14772
Steel flail              14773
Steel flail (noted)      14774
Black flail              14775
Black flail (noted)      14776
White flail              14777
White flail (noted)      14778
Mithril flail            14779
Mithril flail (noted)    14780
Adamant flail            14781
Adamant flail (noted)    14782
Rune flail               14783
Rune flail (noted)       14784
Dragon flail             14785
Dragon flail (noted)     14786

New NPC IDs:
Crazy Archaeologist - 8592
TzRek-Jad - 8594
Pet k'ril tsutsaroth - 8595
Pet kree'arra - 8596
Pet zilyana - 8597
Pet general graardor - 8598

June 6 2022
- Warriors Guild Defender drop rate standardised to 1/50

June 5 2022
- Shooting Star Location Audit
- Overhauled and analysed the shooting star locations, to match authentic data from the era

- Every single shooting star location added regardless of quest requirement (Quest requirement listed in comment next to location)

- East of Dark Wizards' Tower location added

- Jatiszo mine location added

- Lunar Isle mine location added

- Miscellania coal mine location added

- Neitiznot runite mine location added

- Ardougne mining site (Legends Guild) location added

- Kandarin Coal Trucks location added

- Port Khazard mine location added

- Kharidian desert clay mine location added

- Granite and sandstone quarry location added

- Burgh de Rott bank location added

- Mos Le'Harmless bank location added

- Lletya bank location added

- Piscatoris mining site location added

- Wilderness Volcano bank location added

- Wilderness hobgoblin mine location added


June 1 2022
- Pyramid Plunder has been rewritten from the ground-up Authentic treasure rates, proper traps, and rewritten minigame and Sceptre handling make this a top-tier way to gain Thieving EXP and make a fair bit of GP on the side. 

- Shops have been rewritten  
- Shops now restock on a global timer, cleanly, and there is a new toggle in the server config to allow personalised shops, meaning the functionality of every shop will provide a copy of its stock to each individual player, so no one can buy out your coveted vials from the store. The RW server will have this set to false.

- RFD Chest rewrite
- Now doesn't cast a hacky varp to the player on login, and instead checks your Quest Points. If they are over 18, the Chest will appear in the basement. Each chest is individualised for the player, meaning butter is no longer shared commodity.

- Each tier of chest will unlock with another 18 QP, capping out at the moment at tier 4-5.

- Region Specification System  
This provides an abstraction over the region system to reliably build dynamic regions the proper way, even if our underlying implementation changes, which leads me to the below update:

- House Loading has been converted to the RSS Along with this, some old code related to house loading has been removed. Dynamic regions are now being generated more sanely than ever before. Construction is for the most part, stabilised. If you get the "house broke" bullshit, just log out and log back in and it should fix it.  

- Players can now be muted and banned Massive changes to the login, authentication and storage backends, removing unnecessary data fields, and now-unused code.

- Game Ticks are no longer running within a co-routine  
- Now they are no longer running inside of something, this should address apparent server lag/slowdown when server has been active for a long period of time, as well as clean up other potential dead ticks/hangs due to this.

- GRAND EXCHANGE IMPROVEMENTS
- The economy is about to get... interesting. Prices are now pulled directly from inside the GE database's price index table, and prices are now dynamic, and influenced via trading. This can include bots.

- If you leave things in there to pile up over time, on top of the already 10% Bot Tax... it will bite harder to buy if trades do not run often.

- As well: swell changes to its offer algorithm, buying things more efficiently and fulfilling more orders. More things will now go to the highest bidder vs. the one who gets closer to the actual sale price, as-is intended func. for the Grand Exchange.


- The Ring of Duelling has a new teleport! It can now teleport you to Fist of Guthix as intended (27 January 2009)  

- Dragon Full Helm droprate fixes! Trying to obtain a DFH from burning chewed bones at Pyre Sites is now corrected to 1/250, down from 1/500. 

-  Pest Control Veteran Bots!  
- Pest Control Veteran games can now be run and assisted by the bots, allowing you to earn 4 points per successful game. 

- Do note, it is harder than the Intermediate as higher levelled Void Creatures will spawn. Successful games are a coin flip as always, but with a consistent group, you can now play all 3 levels of the Pest Control minigame.

- Venenatis can now drop Ancient Warrior's Equipment/Brawler's gloves  
A bigger target on your back going to fight a boss, for more potential gain. Now there's some more variety outside of Revenants and C. Elemental to risk it for a Statius Warhammer.

- Maximum amount of Revenants actively spawned increased from 20->30
Trying to find the sweetspot of what would be "J###x-like", as currently it is very barren in the Wilderness. Upping this slightly will increase potential risk and add much more needed variety inside the Wilderness, so keep on your toes.

- Additionally, Revenants no longer have a high chance to drop Nothing. Some nonce left the erroneous stacked Nothing(s) inside their drop tables, so they would guaranteed drop charms (charm bug). This has been rectified.

April 22 2022

- The client's OpenGL is now updated from Jgx's legacy 1.X from 2009, to JOGL 2.4.0 (as of 2021, current JOGL). This means a bevy of performance and rendering improvements over the last decade+ for OpenGL, which is a huge boon for modern system access, and allows potential for ports to other systems (such as a native Android client)

- Anti-aliasing support overhauled, allowing for increased AA samples. Client can now utilise 0, 2, 4, 8 and 16 AA samples. config.json customization tab, field "aa_samples": 0, to modify. NOTE: DO NOT USE THE IN-GAME CLIENT ANTI-ALIASING SETTING. Set your sample size within the config.json!!

- Minimap and compass now have a bilinear filter, and hardware and software-based rendering. Increased readability at higher resolutions, and buttery smooth minimap/compass movement, rotation and transitions. config.json customization tab, field "minimap_filter": true/false, to modify.

- Problem/Corrupted SFX are now fixed! Eating, dropping, etc are now correctly playing as they should within the client

- The water shader/rendering is now extended to our increased draw distance limit, making more beautiful seaside and island-bound areas, as well as more beautiful screenshots! 

- VENENATIS ROAMS THE WILDERNESS 🕸️ Venenatis is a Combat level 464 Melee/Magic boss that can stun and cause high combo damage. She roams adjacent to the Wilderness gate, near a mound of spiders. The Treasonous Ring, Wilderness-only Dragon Pickaxe, as well as some decent money/herb drops make her a worthwhile adventure, but be prepared. The combo damage is quite nasty.


- Special Attacks Some special attacks have returned to the game. The weapons: Seercull, Fremennik Blade, and the infamous Darklight's Weaken.

- Jad Practice Mode At a cost, you can challenge Jad directly in combat for no reward. For mechanics, map practice, Jad timings etc. 

- Iban's Staff rises up! 🧙🏿‍♂️ Iban's Staff can now autocast Iban Blast directly. To do this, click the standard Autocast while it is equipped and it should override the spell.

- Lunar Spellbook SFX Overhaul Almost all Lunar spells now have correct GFX and SFX. 📚 🔊 

- Varrock Easy Diary Grand Exchange Teleport Swap You can now talk to Rat Burgiss after completing the Varrock Easy Diary to change your Varrock teleport spell's destination between the city square, and the Grand Exchange.

- Dark Beasts now completely ignore tolerance. Due to a new NPC config flag, Dark Beasts are now infinitely aggressive, as they should be.

- Bot Dialogue Patch 3 Removed a bunch of the cringe frog dialogues, and added another 100+ new dialogue lines. Can you find the one hunting imps? 💢 


08 February -> March 10 2022

- ***The beginning of the Expansion Disk Content*** 
Some of the upcoming items for the Expansion Disk have been successfully ported and packed into the cache.

- New Items:
- Dragon Defender
- Dragon Pickaxe
- Blade of Saeldor and inactive Blade
- Neitiznot Faceguard
- Ring of the Gods
- Treasonous Ring
- Tyrranical Ring
- Corrupt Armour set
- Bronze (g) Armour set
- Iron (g) Armour set
- Steel (g) Armour set
- Mithril (g) Armour set
- Bronze-Mithril (g) Med helms and Square shields

- Dragon pickaxe added to Chaos Elemental and King Black Dragon's droptables at 1/256 and 1/1500 respectively

- Dragon pickaxe added with correct mining power

- As a result of the inclusion of the Dragon Pickaxe, Inferno Adze's mining strength has been reduced back to its original mining strength level, equal to Rune

- Dragon Pickaxe special attack added
- Dragon axe (OSRS) special attack corrected

- Blade of Saeldor has a new, heavily WIP special attack based on the failed poll implementation: Saeldor's Strike, consuming 100% spec energy to stun target

- Bronze, Iron, and Steel (g) equipment now obtainable from Easy Clue reward caskets

- Mithril (g) equipment now obtainable from Medium Clue reward caskets

- Currently unobtainable by player: Neitiznot Faceguard, Blade of Saeldor, Corrupt armour set, Wilderness Rings

- ***Monk's Friend Quest!!**  Should be fully complete now

- Sara brews properly on the linear formula (Meaning they will correctly scale, and correctly overheal to the right amounts)

- Make-All fixed for using Pestle and Mortar on Herblore secondaries

- Dark bow and Crystal bow now have their correct Attack Range of 10 tiles

- Overhauled Entrana Item Restriction checks

-  Allow using a hunter potion to catch implings, you can now Boost to catch them

- Implement Make-X for potatoes, pizzas, and breads

- Improve the behavior of trying to interact with moving entities (such as tool leprechauns)


27 January -> 08 February 2022
- **Fremennik Trials!!** Don your horned helmets and board your longboat, gaining the trust of the standoffish Fremennik tribe won't be easy, but the Adventurer never shies away from a good adventure! The Quest has been fully completed and is now in-game.

- **Tribal Totem!!** Help Kangali Mau recover an important tribal artefact from the clutches of the postal system. The Tribal Totem Quest is now fully complete and in-game.

- **HOSTILE RANDOM EVENTS ARE BACK** Tree Spirit, Rock Golem, River Troll, Shade, and Zombie join evil chicken in his AFK-anihilation schemes. Be careful when training Woodcutting, Mining, and Fishing, as you will have a chance to meet these nasties!

- **A sleeping bot has been awoken!** GenericSlayerBot can only currently mosey his way on down to the Fremennik Slayer Dungeon and kill Cave Crawlers, but he's now awake and online! Go say hi if you see him on a Slayer assigment!

- **GLOBAL SHOP RESTOCKING** Before, shops would only restock if a minute had passed, and a player was in render distance/region distance of the shop. Now, it functions more correctly, restocking shops every 60 seconds globally. Rejoice, ShopScapers!

- **POSITIONAL AUDIO SUPPORT!** Sounds will now fall off after a certain distance. The cacophonous cry of 20+ whips at Pest Control will now be no longer. Monster combat, area sounds, object sounds, and more now correctly fall off depending on how far away from their origin the player is.

- **SFX UPDATES** Due to the inclusion of positional audio, many many SFX have been added. Jewellery teleports(incl. Mounted), Standard Spellbook Teleports, slashing of webs, entering the Mole Lair, digging with a Spade, charging orbs, charging God Spells, Bones to Bananas, Dwarven Multicannon and more are now playing their sounds! Volume UP!

- **MONSTER SFX UPDATES** Due to finding a debug sfx dump, some work has been done to adding monster combat SFX. The list is quite long, so here's one good example: All the Pest Control monsters now have their entire suite of combat SFX!

- **BIRD'S NEST LOOT POOL CORRECTIONS** Seed Nests, Ring Nests, and Wyson nests have been corrected in loot pool, and weights to be more accurate to the original game instead of... a complete mess.

- **CHAOS ELEMENTAL OVERHAUL!** Chaos Elemental has been overhauled with Crash's special Accuracy Polish. He has all SFX added(sans Spellcast SFX), and his drop table has been accurately repaired. He now has his minor drop table, which always rolls alongside his "main" table. No longer will you be only getting 5 bones, you'll get 5 bones *and something possibly decent*. Or 10 Strange Fruit. Who knows?

- **FELLER CELLAR UPDATES!!** The Cellar now shows you what track is currently playing in the chatbox. When a song is about to end, the Cellar will now shuffle, and tell the players currently inside what the next track will be. 2 new tracks have been added to accompany the Cellar Jukebox polish: Fight On! (FF7) and Fun Naming! :cd: 

- Barlak (Long Bone Merchant) now temporarily spawned in the Dorgeshuun Mine, next to the Bone Weapon merchant.

- Falador Mole Lair Music track "The Mad Mole" now correctly plays across the entirety of the Mole Lair.

- Added 8 spawns for Tortured souls, 6 spawns north of the Ectofuntus, and 2 spawns west of Port Phasmatys

- All God Kiteshields now have a +1 Prayer bonus, in line with the previous Equipment Rebalance changes

- All Baby Mole NPCs now correctly wander, and their three unique examine texts have been added.

- Ryan: Failing to enter the GWD Boss rooms now no longer reduces your KC. (Example: attempting to enter the same time as another player is entering)

- Ryan: Plank Make spell rewritten to be authentic and polished

- Ryan: Player is now notified that Turael Slayer tasks won't give them Slayer points

- aweinstock: Add InteractionListener.onUseWithWildcard. Important for quests and content that requires checking of using *any* item, not just small specific handfuls of them.

- aweinstock: Allow Dark Mage to be contacted via NPC Contact Spell, allowing players to contact the mage and repair pouches.

- aweinstock: Make multiple runes continuous (via linear interpolation) between levels required for multiple runes.


18 -> 26 January 2022
- Chaos Druid Warrior drop table corrections, removing copious noted secondary drops (Arios loved these for some reason...)

- Fixed some hats clipping with hair (Q)

- All Ancient Warriors Equipment (Vesta, Zuriel, etc) can now be correctly traded in its un-degraded form

- Kermit: Fixed issues with Adventure bot assembly and initial spawning. You'll notice increased GE activity, as less bots are now capable of becoming inactive null bots, and increased stability on launch, as the adventure bots will no longer all be forced to spawn in the same timeframe.

- Ryan: Fix Gertrude's Cat quest step not appropriately setting, allowing players to once again complete the Quest.

- More Zanaris dialogue and spawn work by Q! 

- **FIRST REDWINGS CACHE CHANGES** - Mounted Glory Right-Click Options, Obelisk in POH
Mounted glories now have right-click options for each teleport, similar to later incarnations, and a brand-new Construction object has been added, built in the Centrepiece slot of the Garden room: The Summoning Obelisk! Requiring 41 Summoning, and similar materials to its later RS2 counterpart, you now have the opportunity to recharge your Summoning points right inside your POH!

- Ryan: Random events now actually random. Before, Randoms would roll every 60 seconds, for everyone, no deviation. Now, there's a larger window (Between 30 and 90 minutes), and is appropriately random so we all don't chimp out at the sight of the first Genie/Old Man random spawn.

- Ryan: Added a bunch of debugging to RSA block and player init

- Ryan: fix ruby bolt(e) calc and fix general bolt spec audio. Rejoice! The Ruby bolt special attack will now also forfeit your eardrums as well as your HP!

- **BOSS SLAYER** Boss Slayer tasks have now been added to the game! You can only receive these Boss tasks from Duradel, and can only get them assigned at 75/90 Slayer depending on Boss. Now you can also get some Slayer EXP while hunting for drops!
75: G.Mole, C.Ele, KBD
90: GWD

- aweinstock: MTA fixes to Graveyard, Telekinetic Maze, and the Shop interface

- aweinstock: All GWD Boss minion secondary drops that should be noted, are now noted.

- aweinstock: TDs unique items have been correctly re-weighted (1/256 for claws, 1/1024 for ruined armour pieces), erroneous highly-weighted Godsword Shard drop on Balfrug Kreeyath corrected

- aweinstock: Add server-wide announcements for Godsword shards, Ruined armour pieces, steam battlestaves and Seercull.

- aweinstock: Re-run charm scraper to fix monster charm drops missed the first time and any regressions since

- **REDWINGS OST DISK 1** Due to new, experimental tools from the sorceror, we can now pack new midi files inside the cache. Ten new songs have been added to the game.



14 -> 18 January 2022
- Long and curved bones are now real content! You can trade these in to Barlak after completion of The Lost Tribe quest, for Construction XP. Huge thanks to Q!

- Many other NPC dialogues by Q in the Dorgeshuun and Ape Atoll areas of the game as well as expansion to Crate code.

- All currently functional Summoning familiars with invisible skills boosts are now correctly added to the game! Pyrelord, Forge Regent, Arctic Bear, Lava Titan, Magpie, Spirit Graahk, Spirit Larupia, Spirit Kyatt, Void Ravager and Wolpertinger now boost skills alongside their other helpful abilities.

- The Mining Guild can now be entered under 60 Mining via the use of skill boosts.

- Black Knight and Water Elemental drop fixes

- The Burnt Stew Incident has been resolved. All burnt bowl foods can now be properly emptied.

- Barrows prayer drain timer has been fixed, and now correctly drains every 18 seconds.

- The Lady of the Lake will now **finally** enhance Excalibur for you if you have the prerequisite Seers Hard Diary and Merlin's Crystal completed, and the headband and sword equipped when talking to her.

- Giant Mole's Burrow ability has now been corrected, no longer being entirely random down to it's last Hitpoint. 

Burrow: When the mole's health is between 5-50%, there is a 25% chance for any incoming attack to cause it to burrow away into another location. There is also a chance that the dirt she digs while burrowing to escape can extinguish the player's light source (shown with dirt splatters on the player's screen), though covered light sources will be unaffected by this.

- Potion decanting note bug should now be fixed and not eat up all your notes when attempting to decant. Another certified Q moment (fixed in !28)

- Fixed incorrect arrows being returned when used with poison++ (Q)
 




13 January 2022
- The GE should now properly save and load, meaning our newly-resurrected bot boys will actually be able to stock up their gathered goods! And, we'll actually be able to put in buy/sell offers that won't get voided on server restart!

- More Vexia-tier dialogue cleanups by Nuggles 😍 

- More dialogue by Q for some Digsite, Isafdar, and Tyras Camp NPCs 🗣️ 

-  aweinstock: Waterbirth Dungeon is cannonable, Spinolyps only drain prayer if they hit, and always target ranged defence. 

- Some nice Waterbirth Dungeon tweaks by Avi, making it more consistent, opening up a new Dag Cannon spot, and Spinolyps draining Prayer more in-line with how it should work on the original games

- aweinstock: - Increase drop rate of Sinister Key to 1/12, make strike spells always max on Salarin, and port Yanille Agility Dungeon to kotlin + InteractionListener.

- Yanille Agility Dungeon's unique content (Salarin and his herb-filled, poison-trapped Chest) has been properly overhauled, and functions correctly to how it does in the RS2 game. Thanks Avi!

11 January 2022
- Core Reversion has now been completed The unstable changes the alien made to the core have been carefully singled out and reverted. Stability should be back to pre-new-core levels of the usual occasional server vomit. 🌍 

- The bots are back in town! A few changes singled out and diff'd by Ryan were found to be the cause of the bots becoming completely nonfunctional. The bots have been returned to their former glory. 🤖  Huge content BIS upgrade!!

- This makes the Adventure bots work similarly to how they did on 09-live pre-core changes, where they will actually adventure and sell shit until the entropy eventually slows them down. Can you find sniffbot?
- This makes the simple gatherer (logs etc) bots work again
- This makes PC bots work again
- This ((probably)) makes the GD bots work again
- This stops most of the bots from pooling at the Lumbridge spawnpoint after they die/void out/other shit

- Playerscripts are functional again! Bit cringe, but they're back for those who want to make use of them

- A staggering amount of new NPC dialogue work done by Q, for places like Falador, Miscellania, and many other areas of the game! 🗣️ 

4 January 2022
- The Ring of Wealth now has **all** of its unique teleports, akin to OSRS. You can now use it to teleport to: Miscellania, Grand Exchange(as usual), Falador Park and Dondakan's Rock. :ring: 

Equipment Rebalancing: Part I
Similar to OSRS's small, oft necessary tweaks to improve health of the combat triangle, and open up some more horizontal or supplementary gearing options, after careful analysis, some of the Rebalance changes have now been included into `redwings`. Read below for all currently included changes. Possibly more to come in the future.

- **ALL** God plate armour (helmets, platebody, platelegs) (Saradomin, Zamorak, Guthix) now receives a `+1` `Prayer bonus`, up from `+0`

- **ALL** God dragonhide (coif, body, chaps, bracers) now receives a `+1` `Prayer bonus`, up from `+0`

- **ALL** God dragonhide chaps `Defence` level requirement has been ***removed***. Def pures weep with reckless abandon.

- Mystic Staves and Battlestaves across the board have had their `Magic attack` and `Magic defence` increased.

- **ALL Battlestaves** have an increased MagicATK/MagicDEF of `+12`, up from `+10`

- **ALL Mystic staves** have an increased MagicATK/MagicDEF of `+14`, up from `+10`

The above staff changes still keep power in-line, as `Ancient Staff` and beyond is at `+15` and above, but opens up more options for staves, making some of the combo staves more useful for combat.

3 January 2022
- Fixes to Family Crest
- GlobalKillCounter fixes
- Ship Charters that cost -1GP now corrected to cost 0GP
- Magic Dart EXP correction
- Can now correctly pickpocket ID 25 Woman
- Enchanting Emerald or Diamond Bracelets no longer turns them into enchanted (noted) bracelets but the correct unnoted item
- A whole bunch of funny backend shit that may or may not work haha (such as disabling GUI server monitor, and reverting Fractals dynamic region changes)
- more fixes to blast furnace (so it doesnt accidentally eat coal when it shouldnt)

31 December 2021
- Renewable Familiars You can now renew your Summoning familiar by Summon-ing an extra pouch of the same Familiar, refreshing their duration to its maximum. Extend Beasts of Burden for longer trips, or your Golem to keep the Mining Boost up!

- Beasts of Burden will now drop their currently held items on dismissal, even if the player is an Iron. 

- Battlestaff price correction to 7k ea 

- Slimy eel fishing spots have been added

- the cope boxes have been disabled we will see him next time

- Clue Scroll Overhaul All monsters that can drop Clue Scrolls have now been corrected to drop the right "clue" drop. No longer will everything always force an Easy Clue. 

27 December 2021

- Secondary banks are now accessible at a hefty fee of 5 million GP, doubling your bank space from 496 to 992. You can switch between bank accounts at a bank teller.

- A new chinning/bursting spot has been made accessible. The Skeleton Monkeys inside Ape Atoll Dungeon can now be fought, and the area is now a multicombat area

- Giant Mole's drop table has been cleaned up, with the Thanos Drop Editor back online.

- The Oo'glog Fresh Meat store is now accessible, and has an increased stock of 100 raw bird meat.

- The damage modifier (A thing mostly used in special attacks to modify damage) is now correctly factored in, for all 3 combat styles.   

- You can now obtain Mort Myre Pears and stems from casting Bloom, and Bloom's sfx has been added.

- The Fishing Guild can now be entered via skill boosts. 

- Edgeville Canoe -> WIlderness Pond travel option now fixed

26 December 2021

- Ruby bolts(e) fix!(kinda)!! Ruby bolt(e) special attack, Blood Forfeit, maximum current Hitpoint damage cap has been increased from 35 to a kiiinda close enough cap of 150 Hitpoints. (The reason why the cap can't be removed yet, is that the Corporeal Beast-specific damage cap is not implemented) 

- The Cider, needed to complete a step in Seers Village Diary, can now be purchased from our bandaid-fix Digsite Exam Center shop. Make sure to take 5, and try not to left-click Drink them

25 December 2021
- Seer's Achievement Diaries should now be fully completable.
- Enhanced Excalibur now has all of it's special attack effects implemented 
- Blast Furnace should now reward 1 smithing XP to everyone in the Blast Furnace area for each bar that is smithed.
- The AFK timer has been removed, may Evil Chicken have mercy on your soul.

- Mobile Client Update
- The Android Client has been updated.
- App strings updated to match the rebrand, new app icon replacement still pending.
- To use redwings-mobile to connect to other servers (such as local dev instances or the main 2009scape game), download and edit the Android Client Config, load the Android Client, tap the rightmost corner in-app to bring up the Settings menu, and tap LOAD CONFIG. 

24 December 2021
- THE FUNNY FURNACE UPDATE(and friends)
- What's new?
- Blast Furnace is now complete enough to be considered complete™️
- Ordan and Jorzik's dialogue has been implemented.
- Jorzik's armour store is in as well so now you have a place to dump all of your blast furnace creations
- Some of the funny furnace funny logic has been polished up and the kinks worked out
- The steps to the blast furnace in Keldagrim will now gatekeep players with under 60 smithing as intended
- If you're under 60 smithing you can pay 2500gp for ten minutes of access to the blast furnace
- If you have the Ring of Charos(a) equipped the fee is lowered to 1250gp
- Green Dragon bots can hop over the wildy ditch again but bots in general are still fucked
- The correct item spawns have been added to the Blast Furnace area
- The Blast Furnace will now remember what you have stored into it
- You now have to pedal your ores into the ore pot before you can smelt them

- Explorer's Ring now has correct sfx for recharging your run energy, as well as the Falador Farm Cabbage-port. Leaf-bladed sword now has correct sfx.

- Family Crest Update! Fractals polished the last bits of the quest up. The bossfight, Chronozon, now works as it should, and the perfect gold ore puzzle has been added.

- Client Update!
1. december_snow is now accessible, allowing you to toggle on/off the game-wide snow effect. 
2. left_click_attack This allows you, no matter what, to have any monster's main left-click option be Attack. This is important for bossing, as bosses usually have insanely high Combat levels, thus usually only being right-clickable.  

23 December 2021 
- The Xmas Gift Limit has been increased! To compensate for missing most of December, the daily gift limit has been increased to 28.

- Permadeath Ironmen should now be able to access the 20x rates from Hans in Tutorial Cellar/Lumbridge Castle. Take on the challenge, and rebuild faster than ever.  

- sniffbot is back The Adventure Bot dialogue has been restored

- At the Digsite Exam Centre, south of the Digsite, there is a Researcher you can talk to to access a shop "Forgotten Relix" which contains until now inacessible quest-unlocked items such as the Magic Secateurs, Salve Amulet, and Iban's Staff.  

- Bill Teach, Once Lost Uncle of the Fellers, has returned to take us on adventures near and far. At the Port Phasmatys pub, you can talk to him and he will take you where you want to go.  

- Barrows gloves return from the crypt! The Culinaromancer's Chest, the RFD gloves, and the bankchest in Lumbridge Basement are now accessible to all accounts.

- Blast Furnace Exclusive Beta The Blast Furnace is now accessible deep within Keldagrim. Smithing skill trainers are weeping, overjoyed. 