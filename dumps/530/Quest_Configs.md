<p>Welcome to the Quest Page. Here you can find a consolidated list of quests, their configs and the status of implementation within the 2009Scape Server. The <strong>last</strong> quest released in this server's revision was <strong>"In Pyre Need" on January 6th, 2009</strong>.</p>
<p><strong>Quests are listed in reverse chronological order.</strong></p>
<hr>
<p>Legend:</p>
<ul>
<li>'' (Blank), the quest has not been implemented yet</li>
<li>In progress, some elements of the quest have been written, but is not finished</li>
<li>In-game, bugs and inaccuracy may be visible (Most implemented quests have been carried over from another base)</li>
<li>Complete, minor/no bugs and accurate to the 2009 version of RuneScape</li>
</ul>
<hr>
<p>Have a config not listed here? Feel free to update this page or let us know on the <a href="https://discord.gg/4zrA2Wy" rel="nofollow">RSRebotted Discord</a></p>
<hr>
<table role="table">
<thead>
<tr>
<th>Quest Number</th>
<th>Quest Name</th>
<th>Config ID</th>
<th>Not Started</th>
<th>Started</th>
<th>Completed</th>
<th>In-game Status</th>
<th>ButtonID</th>
</tr>
</thead>
<tbody>
<tr>
<td>149</td>
<td>In Pyre Need</td>
<td>1370</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>161</td>
</tr>
<tr>
<td>148</td>
<td>Myths of the White Lands</td>
<td>1365</td>
<td>0</td>
<td>1</td>
<td>60</td>
<td></td>
<td>162</td>
</tr>
<tr>
<td>147</td>
<td>While Guthix Sleeps</td>
<td>1330</td>
<td>0</td>
<td>1</td>
<td>901</td>
<td></td>
<td>160</td>
</tr>
<tr>
<td>146</td>
<td>Swept Away</td>
<td>1327</td>
<td>0</td>
<td>1</td>
<td>63</td>
<td></td>
<td>159</td>
</tr>
<tr>
<td>145</td>
<td>Defender of Varrock</td>
<td>1311</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>158</td>
</tr>
<tr>
<td>144</td>
<td>Summer's End</td>
<td>1306</td>
<td>0</td>
<td>1</td>
<td>99</td>
<td></td>
<td>157</td>
</tr>
<tr>
<td>143</td>
<td>All Fired Up</td>
<td>1282</td>
<td>0</td>
<td>1</td>
<td>90</td>
<td></td>
<td>156</td>
</tr>
<tr>
<td>142</td>
<td>Meeting History</td>
<td>1274</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>155</td>
</tr>
<tr>
<td>141</td>
<td>Spirit of Summer</td>
<td>1271</td>
<td>0</td>
<td>1</td>
<td>80</td>
<td></td>
<td>154</td>
</tr>
<tr>
<td>-</td>
<td>Learning the Ropes</td>
<td>281</td>
<td>0</td>
<td>1</td>
<td>1000</td>
<td>In Progress</td>
<td>5</td>
</tr>
<tr>
<td>140</td>
<td>Rocking Out</td>
<td>1236</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>153</td>
</tr>
<tr>
<td>139</td>
<td>Smoking Kills</td>
<td>1232</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>152</td>
</tr>
<tr>
<td>138</td>
<td>TokTz-Ket-Dill</td>
<td>1227</td>
<td>0</td>
<td>1</td>
<td>63</td>
<td></td>
<td>151</td>
</tr>
<tr>
<td>137</td>
<td>Perils of Ice Mountain</td>
<td>1225</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>150</td>
</tr>
<tr>
<td>136</td>
<td>Legacy of Seergaze</td>
<td>1218</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>149</td>
</tr>
<tr>
<td>135</td>
<td>Kennith's Concerns</td>
<td>1204</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>148</td>
</tr>
<tr>
<td>134</td>
<td>Catapult Construction</td>
<td>1190</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>147</td>
</tr>
<tr>
<td>133</td>
<td>As a First Resort...</td>
<td>1184</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>146</td>
</tr>
<tr>
<td>132</td>
<td>Wolf Whistle</td>
<td>1178</td>
<td>0</td>
<td>5</td>
<td>220</td>
<td>In-game</td>
<td>145</td>
</tr>
<tr>
<td>131</td>
<td>Dealing with Scabaras</td>
<td>1165</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>144</td>
</tr>
<tr>
<td>130</td>
<td>Land of the Goblins</td>
<td>1094</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>143</td>
</tr>
<tr>
<td>129</td>
<td>Back to my Roots</td>
<td>1077</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>142</td>
</tr>
<tr>
<td>128</td>
<td>The Path of Glouphrie</td>
<td>1063</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>141</td>
</tr>
<tr>
<td>127</td>
<td>King's Ransom</td>
<td>1049</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>140</td>
</tr>
<tr>
<td>126</td>
<td>Grim Tales</td>
<td>1016</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>139</td>
</tr>
<tr>
<td>125</td>
<td>Dream Mentor</td>
<td>1003</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>138</td>
</tr>
<tr>
<td>124</td>
<td>Another Slice of H.A.M.</td>
<td>997</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>137</td>
</tr>
<tr>
<td>123</td>
<td>Olaf's Quest</td>
<td>994</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>136</td>
</tr>
<tr>
<td>122</td>
<td>What Lies Below</td>
<td>992</td>
<td>0</td>
<td>1</td>
<td>150</td>
<td>In-game</td>
<td>135</td>
</tr>
<tr>
<td>121</td>
<td>The Great Brain Robbery</td>
<td>980</td>
<td>0</td>
<td>1</td>
<td>130</td>
<td>Unstarted</td>
<td>134</td>
</tr>
<tr>
<td>120</td>
<td>Tower of Life</td>
<td>977</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>133</td>
</tr>
<tr>
<td>119</td>
<td>The Fremennik Isles</td>
<td>970</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td>development</td>
<td>132</td>
</tr>
<tr>
<td>118</td>
<td>Cold War</td>
<td>968</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>131</td>
</tr>
<tr>
<td>117</td>
<td>Contact!</td>
<td>964</td>
<td>0</td>
<td>1</td>
<td>130</td>
<td></td>
<td>38</td>
</tr>
<tr>
<td>116</td>
<td>Animal Magnetism</td>
<td>939</td>
<td>0</td>
<td>1</td>
<td>240</td>
<td>In-game</td>
<td>32</td>
</tr>
<tr>
<td>115</td>
<td>Eagles' Peak</td>
<td>934</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>50</td>
</tr>
<tr>
<td>114</td>
<td>Enlightened Journey</td>
<td>912</td>
<td>0</td>
<td>1</td>
<td>200</td>
<td></td>
<td>54</td>
</tr>
<tr>
<td>113</td>
<td>My Arm's Big Adventure</td>
<td>905</td>
<td>0</td>
<td>1</td>
<td>311</td>
<td></td>
<td>93</td>
</tr>
<tr>
<td>112</td>
<td>Elemental Workshop II</td>
<td>896</td>
<td>0</td>
<td>1</td>
<td>11</td>
<td></td>
<td>52</td>
</tr>
<tr>
<td>111</td>
<td>Slug Menace</td>
<td>874</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>109</td>
</tr>
<tr>
<td>110</td>
<td>Darkness of Hallowvale</td>
<td>869</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>41</td>
</tr>
<tr>
<td>109</td>
<td>The Eyes of Glouphrie</td>
<td>844</td>
<td>0</td>
<td>1</td>
<td>60</td>
<td></td>
<td>55</td>
</tr>
<tr>
<td>108</td>
<td>Lunar Diplomacy</td>
<td>823</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>84</td>
</tr>
<tr>
<td>107</td>
<td>A Fairy Tale Part II</td>
<td>810</td>
<td>0</td>
<td>1</td>
<td>81</td>
<td></td>
<td>57</td>
</tr>
<tr>
<td>106</td>
<td>Death to the Dorgeshuun</td>
<td>794</td>
<td>0</td>
<td>1</td>
<td>13</td>
<td></td>
<td>42</td>
</tr>
<tr>
<td>105</td>
<td>Royal Trouble</td>
<td>730</td>
<td>0</td>
<td>1</td>
<td>21</td>
<td></td>
<td>105</td>
</tr>
<tr>
<td>104</td>
<td>Swan Song</td>
<td>723</td>
<td>0</td>
<td>1</td>
<td>200</td>
<td></td>
<td>116</td>
</tr>
<tr>
<td>103</td>
<td>Rag and Bone Man</td>
<td>714</td>
<td>0</td>
<td>1</td>
<td>4</td>
<td></td>
<td>99</td>
</tr>
<tr>
<td>102</td>
<td>A Soul's Bane</td>
<td>709</td>
<td>0</td>
<td>1</td>
<td>1261</td>
<td>development</td>
<td>114</td>
</tr>
<tr>
<td>101</td>
<td>In Aid of the Myreque</td>
<td>705</td>
<td>0</td>
<td>1</td>
<td>16293</td>
<td></td>
<td>78</td>
</tr>
<tr>
<td>100</td>
<td>Recipe for Disaster</td>
<td>678</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>101</td>
</tr>
<tr>
<td>99</td>
<td>A Fairy Tale Part I</td>
<td>671</td>
<td>0</td>
<td>1</td>
<td>218</td>
<td></td>
<td>56</td>
</tr>
<tr>
<td>98</td>
<td>Cabin Fever</td>
<td>655</td>
<td>0</td>
<td>1</td>
<td>131</td>
<td></td>
<td>36</td>
</tr>
<tr>
<td>97</td>
<td>Enakhra's Lament</td>
<td>641</td>
<td>0</td>
<td>1</td>
<td>70</td>
<td></td>
<td>53</td>
</tr>
<tr>
<td>96</td>
<td>The Hand in the Sand</td>
<td>635</td>
<td>0</td>
<td>1</td>
<td>160</td>
<td></td>
<td>71</td>
</tr>
<tr>
<td>95</td>
<td>Devious Minds</td>
<td>622</td>
<td>0</td>
<td>1</td>
<td>80</td>
<td></td>
<td>45</td>
</tr>
<tr>
<td>94</td>
<td>Spirits of the Elid</td>
<td>616</td>
<td>0</td>
<td>1</td>
<td>60</td>
<td></td>
<td>115</td>
</tr>
<tr>
<td>93</td>
<td>Ratcatchers</td>
<td>607</td>
<td>0</td>
<td>1</td>
<td>127</td>
<td>Unstarted</td>
<td>100</td>
</tr>
<tr>
<td>92</td>
<td>Making History</td>
<td>604</td>
<td>0</td>
<td>1</td>
<td>4</td>
<td></td>
<td>85</td>
</tr>
<tr>
<td>91</td>
<td>Shadow of the Storm</td>
<td>602</td>
<td>0</td>
<td>1</td>
<td>125</td>
<td></td>
<td>111</td>
</tr>
<tr>
<td>90</td>
<td>Rum Deal</td>
<td>600</td>
<td>0</td>
<td>1</td>
<td>19</td>
<td>Unstarted</td>
<td>106</td>
</tr>
<tr>
<td>89</td>
<td>Mourning's Ends Part II</td>
<td>574</td>
<td>0</td>
<td>9</td>
<td>60</td>
<td></td>
<td>91</td>
</tr>
<tr>
<td>88</td>
<td>Wanted!</td>
<td>571</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>129</td>
</tr>
<tr>
<td>87</td>
<td>A Tail of Two Cats</td>
<td>568</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>118</td>
</tr>
<tr>
<td>86</td>
<td>Garden of Tranquility</td>
<td>553</td>
<td>0</td>
<td>1</td>
<td>60</td>
<td>Unstarted</td>
<td>65</td>
</tr>
<tr>
<td>85</td>
<td>Forgettable Tale of a Drunken Dwarf</td>
<td>521</td>
<td>0</td>
<td>1</td>
<td>140</td>
<td></td>
<td>62</td>
</tr>
<tr>
<td>84</td>
<td>Mourning's Ends Part I</td>
<td>517</td>
<td>0</td>
<td>1</td>
<td>8</td>
<td>Unstarted</td>
<td>90</td>
</tr>
<tr>
<td>83</td>
<td>Recruitment Drive</td>
<td>496</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td></td>
<td>102</td>
</tr>
<tr>
<td>82</td>
<td>The Giant Dwarf</td>
<td>482</td>
<td>0</td>
<td>1</td>
<td>50</td>
<td></td>
<td>68</td>
</tr>
<tr>
<td>81</td>
<td>The Lost Tribe</td>
<td>465</td>
<td>0</td>
<td>1</td>
<td>11</td>
<td></td>
<td>83</td>
</tr>
<tr>
<td>80</td>
<td>Zogre Flesh Eaters</td>
<td>455</td>
<td>0</td>
<td>1</td>
<td>461</td>
<td></td>
<td>39</td>
</tr>
<tr>
<td>79</td>
<td>Tears of Guthix</td>
<td>449</td>
<td>0</td>
<td>1</td>
<td>42</td>
<td>40 also shows unfinished</td>
<td>119</td>
</tr>
<tr>
<td>78</td>
<td>Icthlarin's Little Helper</td>
<td>445</td>
<td>0</td>
<td>1</td>
<td>26</td>
<td></td>
<td>77</td>
</tr>
<tr>
<td>77</td>
<td>Desert Treasure</td>
<td>440</td>
<td>0</td>
<td>1</td>
<td>15</td>
<td></td>
<td>44</td>
</tr>
<tr>
<td>76</td>
<td>The Golem</td>
<td>437</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td></td>
<td>69</td>
</tr>
<tr>
<td>75</td>
<td>The Feud</td>
<td>435</td>
<td>0</td>
<td>1</td>
<td>28</td>
<td></td>
<td>59</td>
</tr>
<tr>
<td>74</td>
<td>Between a Rock...</td>
<td>433</td>
<td>0</td>
<td>1</td>
<td>110</td>
<td></td>
<td>33</td>
</tr>
<tr>
<td>73</td>
<td>Mountain Daughter</td>
<td>423</td>
<td>0</td>
<td>1</td>
<td>70</td>
<td></td>
<td>89</td>
</tr>
<tr>
<td>72</td>
<td>One Small Favour</td>
<td>416</td>
<td>0</td>
<td>1</td>
<td>276</td>
<td>Unstarted</td>
<td>96</td>
</tr>
<tr>
<td>71</td>
<td>Ghosts Ahoy</td>
<td>408</td>
<td>0</td>
<td>2^30</td>
<td>-1</td>
<td></td>
<td>67</td>
</tr>
<tr>
<td>70</td>
<td>Roving Elves</td>
<td>402</td>
<td>0</td>
<td>1</td>
<td>6</td>
<td>In-game</td>
<td>104</td>
</tr>
<tr>
<td>69</td>
<td>Creature of Fenkenstrain</td>
<td>399</td>
<td>0</td>
<td>1</td>
<td>7</td>
<td>Unstarted Config 400 also turns quest yellow</td>
<td>40</td>
</tr>
<tr>
<td>68</td>
<td>In Search of the Myreque</td>
<td>387</td>
<td>0</td>
<td>1</td>
<td>105</td>
<td>Unstarted</td>
<td>79</td>
</tr>
<tr>
<td>67</td>
<td>Troll Romance</td>
<td>385</td>
<td>0</td>
<td>1</td>
<td>45</td>
<td></td>
<td>126</td>
</tr>
<tr>
<td>66</td>
<td>Haunted Mine</td>
<td>382</td>
<td>0</td>
<td>1</td>
<td>11</td>
<td>Unstarted</td>
<td>72</td>
</tr>
<tr>
<td>65</td>
<td>Monkey Madness</td>
<td>365</td>
<td>0</td>
<td>1</td>
<td>9</td>
<td>Unstarted</td>
<td>87</td>
</tr>
<tr>
<td>64</td>
<td>Throne of Miscellania</td>
<td>359</td>
<td>0</td>
<td>1</td>
<td>100</td>
<td></td>
<td>121</td>
</tr>
<tr>
<td>63</td>
<td>Horror from the Deep</td>
<td>351</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td></td>
<td>76</td>
</tr>
<tr>
<td>62</td>
<td>The Fremennik Trials</td>
<td>347</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td>Unstarted</td>
<td>63</td>
</tr>
<tr>
<td>61</td>
<td>Shades of Mort'ton</td>
<td>339</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>110</td>
</tr>
<tr>
<td>60</td>
<td>Eadgar's Ruse</td>
<td>335</td>
<td>0</td>
<td>1</td>
<td>110</td>
<td>Unstarted</td>
<td>49</td>
</tr>
<tr>
<td>59</td>
<td>Regicide</td>
<td>328</td>
<td>0</td>
<td>1</td>
<td>15</td>
<td></td>
<td>103</td>
</tr>
<tr>
<td>58</td>
<td>Tai Bwo Wannai Trio</td>
<td>320</td>
<td>0</td>
<td>3</td>
<td>?</td>
<td></td>
<td>117</td>
</tr>
<tr>
<td>57</td>
<td>Troll Stronghold</td>
<td>317</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>127</td>
</tr>
<tr>
<td>56</td>
<td>Death Plateau</td>
<td>314</td>
<td>0</td>
<td>1</td>
<td>80</td>
<td></td>
<td>43</td>
</tr>
<tr>
<td>55</td>
<td>Nature Spirit</td>
<td>307</td>
<td>0</td>
<td>1</td>
<td>110</td>
<td>Unstarted</td>
<td>94</td>
</tr>
<tr>
<td>54</td>
<td>Priest In Peril</td>
<td>302</td>
<td>0</td>
<td>1</td>
<td>100</td>
<td>In-game</td>
<td>98</td>
</tr>
<tr>
<td>53</td>
<td>Elemental Workshop I</td>
<td>299</td>
<td>0</td>
<td>1</td>
<td>-1</td>
<td></td>
<td>51</td>
</tr>
<tr>
<td>52</td>
<td>Big Chompy Bird Hunting</td>
<td>293</td>
<td>0</td>
<td>1</td>
<td>65</td>
<td></td>
<td>34</td>
</tr>
<tr>
<td>51</td>
<td>Rune Mysteries</td>
<td>63</td>
<td>0</td>
<td>1</td>
<td>6</td>
<td>In-game</td>
<td>26</td>
</tr>
<tr>
<td>50</td>
<td>Legends Quest</td>
<td>139</td>
<td>0</td>
<td>1</td>
<td>75</td>
<td>Unstarted</td>
<td>81</td>
</tr>
<tr>
<td>49</td>
<td>Gertrude's Cat</td>
<td>180</td>
<td>0</td>
<td>1</td>
<td>100</td>
<td>In-game</td>
<td>66</td>
</tr>
<tr>
<td>48</td>
<td>The Dig Site</td>
<td>131</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td>Unstarted</td>
<td>46</td>
</tr>
<tr>
<td>47</td>
<td>Murder Mystery</td>
<td>192</td>
<td>0</td>
<td>1</td>
<td>2</td>
<td>Unstarted</td>
<td>92</td>
</tr>
<tr>
<td>46</td>
<td>Dwarf Cannon</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>11</td>
<td>In-game</td>
<td>48</td>
</tr>
<tr>
<td>45</td>
<td>Watch Tower</td>
<td>212</td>
<td>0</td>
<td>1</td>
<td>13</td>
<td>Unstarted</td>
<td>130</td>
</tr>
<tr>
<td>44</td>
<td>The Tourist Trap</td>
<td>197</td>
<td>0</td>
<td>1</td>
<td>30</td>
<td>In-game</td>
<td>122</td>
</tr>
<tr>
<td>43</td>
<td>Observatory Quest</td>
<td>112</td>
<td>0</td>
<td>1</td>
<td>7</td>
<td></td>
<td>95</td>
</tr>
<tr>
<td>42</td>
<td>Underground Pass</td>
<td>162</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>128</td>
</tr>
<tr>
<td>41</td>
<td>Shilo Village</td>
<td>116</td>
<td>0</td>
<td>1</td>
<td>15</td>
<td></td>
<td>113</td>
</tr>
<tr>
<td>40</td>
<td>The Grand Tree</td>
<td>150</td>
<td>0</td>
<td>1</td>
<td>160</td>
<td>Unstarted</td>
<td>70</td>
</tr>
<tr>
<td>39</td>
<td>Jungle Potion</td>
<td>175</td>
<td>0</td>
<td>1</td>
<td>12</td>
<td>In-game</td>
<td>80</td>
</tr>
<tr>
<td>38</td>
<td>Biohazard</td>
<td>68</td>
<td>0</td>
<td>1</td>
<td>16</td>
<td></td>
<td>35</td>
</tr>
<tr>
<td>37</td>
<td>Waterfall Quest</td>
<td>65</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td>In-game</td>
<td>64</td>
</tr>
<tr>
<td>36</td>
<td>Sea Slug</td>
<td>159</td>
<td>0</td>
<td>1</td>
<td>12</td>
<td></td>
<td>108</td>
</tr>
<tr>
<td>35</td>
<td>Plague City</td>
<td>165</td>
<td>0</td>
<td>1</td>
<td>29</td>
<td></td>
<td>97</td>
</tr>
<tr>
<td>34</td>
<td>Sheep Herder</td>
<td>60</td>
<td>0</td>
<td>1</td>
<td>3</td>
<td>In-game</td>
<td>112</td>
</tr>
<tr>
<td>33</td>
<td>Hazeel Cult</td>
<td>223</td>
<td>0</td>
<td>1</td>
<td>9</td>
<td>Unstarted</td>
<td>73</td>
</tr>
<tr>
<td>32</td>
<td>Fight Arena</td>
<td>17</td>
<td>0</td>
<td>1</td>
<td>14</td>
<td>Unstarted</td>
<td>60</td>
</tr>
<tr>
<td>31</td>
<td>Tree Gnome Village</td>
<td>111</td>
<td>0</td>
<td>1</td>
<td>9</td>
<td></td>
<td>124</td>
</tr>
<tr>
<td>30</td>
<td>Holy Grail</td>
<td>5</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td>Unstarted</td>
<td>75</td>
</tr>
<tr>
<td>29</td>
<td>Clock Tower</td>
<td>10</td>
<td>0</td>
<td>1</td>
<td>8</td>
<td></td>
<td>37</td>
</tr>
<tr>
<td>28</td>
<td>Temple of Ikov</td>
<td>26</td>
<td>0</td>
<td>1</td>
<td>80</td>
<td></td>
<td>120</td>
</tr>
<tr>
<td>27</td>
<td>Monk's Friend</td>
<td>30</td>
<td>0</td>
<td>1</td>
<td>80</td>
<td>Files are somewhere on the Discord</td>
<td>88</td>
</tr>
<tr>
<td>26</td>
<td>Fishing Contest</td>
<td>11</td>
<td>0</td>
<td>1</td>
<td>5</td>
<td>In-game</td>
<td>61</td>
</tr>
<tr>
<td>25</td>
<td>Tribal Totem</td>
<td>200</td>
<td>0</td>
<td>1</td>
<td>5</td>
<td></td>
<td>125</td>
</tr>
<tr>
<td>24</td>
<td>Family Crest</td>
<td>148</td>
<td>0</td>
<td>1</td>
<td>11</td>
<td>Unstarted</td>
<td>58</td>
</tr>
<tr>
<td>23</td>
<td>Scorpion Catcher</td>
<td>76</td>
<td>0</td>
<td>1</td>
<td>?</td>
<td></td>
<td>107</td>
</tr>
<tr>
<td>22</td>
<td>Heroes Quest</td>
<td>188</td>
<td>0</td>
<td>1</td>
<td>15</td>
<td>Unstarted</td>
<td>74</td>
</tr>
<tr>
<td>21</td>
<td>Merlin's Crystal</td>
<td>14</td>
<td>0</td>
<td>1</td>
<td>7</td>
<td>In-game</td>
<td>86</td>
</tr>
<tr>
<td>20</td>
<td>Witch's House</td>
<td>226</td>
<td>0</td>
<td>1</td>
<td>7</td>
<td>In-game</td>
<td>123</td>
</tr>
<tr>
<td>19</td>
<td>Lost City</td>
<td>147</td>
<td>0</td>
<td>1</td>
<td>6</td>
<td>In-game</td>
<td>83</td>
</tr>
<tr>
<td>18</td>
<td>Druidic Ritual</td>
<td>80</td>
<td>0</td>
<td>3</td>
<td>4</td>
<td>In-game</td>
<td>47</td>
</tr>
<tr>
<td>17</td>
<td>Dragon Slayer</td>
<td>176</td>
<td>0</td>
<td>1</td>
<td>10</td>
<td>In-game</td>
<td>17</td>
</tr>
<tr>
<td>16</td>
<td>Pirate's Treasure</td>
<td>71</td>
<td>0</td>
<td>1</td>
<td>4</td>
<td>In-game</td>
<td>22</td>
</tr>
<tr>
<td>15</td>
<td>Goblin Diplomacy</td>
<td>62</td>
<td>0</td>
<td>1</td>
<td>6</td>
<td>In-game</td>
<td>19</td>
</tr>
<tr>
<td>14</td>
<td>Knight's Sword</td>
<td>122</td>
<td>0</td>
<td>1</td>
<td>7</td>
<td>In-game</td>
<td>21</td>
</tr>
<tr>
<td>13</td>
<td>Witches Potion</td>
<td>67</td>
<td>0</td>
<td>1</td>
<td>3</td>
<td>In-game</td>
<td>30</td>
</tr>
<tr>
<td>12</td>
<td>Black Knights' Fortress</td>
<td>130</td>
<td>0</td>
<td>1</td>
<td>4</td>
<td>In-game</td>
<td>13</td>
</tr>
<tr>
<td>11</td>
<td>Doric's Quest</td>
<td>31</td>
<td>0</td>
<td>1</td>
<td>100</td>
<td>In-game</td>
<td>16</td>
</tr>
<tr>
<td>10</td>
<td>Prince Ali Rescue</td>
<td>273</td>
<td>0</td>
<td>1</td>
<td>110</td>
<td>In-game</td>
<td>23</td>
</tr>
<tr>
<td>9</td>
<td>Imp Catcher</td>
<td>160</td>
<td>0</td>
<td>1</td>
<td>2</td>
<td>In-game</td>
<td>20</td>
</tr>
<tr>
<td>8</td>
<td>Vampire Slayer</td>
<td>178</td>
<td>0</td>
<td>1</td>
<td>3</td>
<td>In-game</td>
<td>29</td>
</tr>
<tr>
<td>7</td>
<td>Romeo &amp; Juliet</td>
<td>144</td>
<td>0</td>
<td>1</td>
<td>100</td>
<td>In-game</td>
<td>25</td>
</tr>
<tr>
<td>6</td>
<td>Shield of Arrav</td>
<td>145</td>
<td>0</td>
<td>1</td>
<td>7</td>
<td>In-game</td>
<td>28</td>
</tr>
<tr>
<td>5</td>
<td>Sheep Shearer</td>
<td>179</td>
<td>0</td>
<td>1</td>
<td>21</td>
<td>In-game</td>
<td>27</td>
</tr>
<tr>
<td>4</td>
<td>The Restless Ghost</td>
<td>107</td>
<td>0</td>
<td>1</td>
<td>5</td>
<td>In-game</td>
<td>24</td>
</tr>
<tr>
<td>3</td>
<td>Ernest the Chicken</td>
<td>32</td>
<td>0</td>
<td>1</td>
<td>3</td>
<td>In-game</td>
<td>18</td>
</tr>
<tr>
<td>2</td>
<td>Demon Slayer</td>
<td>222</td>
<td>0</td>
<td>1</td>
<td>3</td>
<td>In-game</td>
<td>15</td>
</tr>
<tr>
<td>1</td>
<td>Cook's Assistant</td>
<td>29</td>
<td>0</td>
<td>1</td>
<td>2</td>
<td>In-game</td>
<td>14</td>
</tr>
</tbody>
</table>