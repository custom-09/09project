package rs09.game.content.dialogue.region.rellekka.mountaincamp

import api.isQuestComplete
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

@Initializable
class RagnarDialogue(player: Player? = null) : DialoguePlugin(player){

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        if(isQuestComplete(player, "Mountain Daughter")) {
            playerl(FacialExpression.FRIENDLY, "I have good news. I have been to the afterlife and Asleif is there, happy and peaceful.")
        } else {
            //needs dialogue before quest is completed
        }
        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> npc(FacialExpression.HAPPY, "Thank you, friend. This is great news.").also { stage = END_DIALOGUE }
        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return RagnarDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.RAGNAR_1808)
    }

}