package rs09.game.content.dialogue.region.tearsofguthixcavern

import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import org.rs09.consts.NPCs
import rs09.game.content.dialogue.DialogueFile
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

class Juna_You_Tell_Me_A_Story_1() : DialogueFile() {

    val n = FacialExpression.NEUTRAL

    override fun handle(componentID: Int, buttonID: Int) {
        npc = NPC(NPCs.JUNA_2023)

        when (stage) {
            0 -> npc(n, "I will tell you the story of the light-creatures.").also { stage++ }

            1 -> npcl(n, "Myriad and beautiful were the creatures and civilizations of the " +
                    "early ages of the world. Gielinor was a work of art, shaped lovingly over " +
                    "the millennia by the creative mind of Guthix.").also { stage++ }

            2 -> npcl(n, "Only the sturdiest races survived the God Wars, and even then " +
                    "by only abandoning their high culture and gearing the societies towards " +
                    "war. Of the more delicate races there is now no trace, and almost no memory.").also { stage++ }

            3 -> npcl(n, "One such race had bodies as fragile as snowflakes, yet they built" +
                    " crystal cities that stood for a thousand years.").also { stage++ }

            4 -> npcl(n, "The wind would whisper through the spires and fill them with " +
                    "street harmonies, and the rising sun would shine through the precious " +
                    "gems that studded the towers and create inter players of light as if " +
                    "rainbows were dancing.").also { stage++ }

            5 -> npcl(n, "Indeed, so marvellous was this light-show at it's height that " +
                    "the patterns of light themselves became alive, and the great flocks of " +
                    "luminous creatures rode along the gem-cast beams, each drawn to its own " +
                    "colour.").also { stage++ }

            6 -> npcl(n, "The creatures you see floating in this chasam are the last " +
                    "sorry remnants of that age. I do not know how they made their way here " +
                    "and survived to this time, but I am grateful for their company.").also { stage = END_DIALOGUE }

        }
    }
}