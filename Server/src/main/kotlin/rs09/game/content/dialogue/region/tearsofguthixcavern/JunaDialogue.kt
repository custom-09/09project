package rs09.game.content.dialogue.region.tearsofguthixcavern

import api.getAttribute
import api.openDialogue
import api.setAttribute
import core.game.content.dialogue.DialoguePlugin
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable
import org.rs09.consts.NPCs
import rs09.tools.END_DIALOGUE

@Initializable
class JunaDialogue(player: Player? = null) : DialoguePlugin(player){

    val f = FacialExpression.FRIENDLY
    val a = FacialExpression.ASKING
    val n = FacialExpression.NEUTRAL

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC

        if (!getAttribute(player, "juna:spoken-to-first-time", false)) {
            npc(FacialExpression.FRIENDLY,"Tell me... a story...").also { stage = 0 }
        }



        return true
    }

    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        when (stage) {
            0 -> player(f, "A story?").also { stage++ }
            1 -> npcl(n, "I have been waiting here three thousand years, guarding the Tears" +
                    " of Guthix. I serve my master faithfully, but I am bored.").also { stage++ }

            2 -> npcl(n, "An adventurer such as yourself must have many tales to tell. If you can " +
                    "entertain me, I will let you in the cave for a time.").also { stage++ }
            3 -> npcl(n, "The more I enjoy your story, the more time I will give you in the cave.").also { stage++ }
            4 -> {
                setAttribute(player, "/save:juna:spoken-to-first-time", true)
                npcl(n, "Then you can drink the power of balance, which will make you stronger in whatever area you are weakest.").also { stage++ }
            }

            5 -> {
                if (getAttribute(player, "juna:explained-what-tog-are", false)) {
                    options("Okay...", "Not now", "What are the Tears of Guthix?").also { stage++ }
                } else {
                    options("Okay...", "Not now", "What are the Tears of Guthix?", "You tell me a story").also { stage++ }
                }
            }
            6 -> when (buttonId) {
                2 -> player(n, "Not now.").also { stage = END_DIALOGUE }
                3 -> player(a, "What are the Tears of Guthix?").also { openDialogue(player, Juna_ToG_What_Are_The_Tears_DialogueFile()) }
                4 -> options("Tell me about the Tears of Guthix.", "Tell me a new story.", "Tell me a new story.").also { stage = 10 }
            }

            10 -> when(buttonId) {
                1 -> openDialogue(player, Juna_ToG_What_Are_The_Tears_DialogueFile())
                2 -> openDialogue(player, Juna_You_Tell_Me_A_Story_1())
                3 -> openDialogue(player, Juna_You_Tell_Me_A_Story_2())
            }

        }
        return true
    }

    override fun newInstance(player: Player?): DialoguePlugin {
        return JunaDialogue(player)
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.JUNA_2023)
    }

}