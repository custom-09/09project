## Table of Contents

* [Contributing](#contributing)
* [Setup for Content Developers](#content-developers-setting-up-the-project)
  * [Github Setup](#github-setup)
  * [Prerequisites](#prerequisites)
  * [Prereq Installation](#prereq-installation)
  * [Project Setup](#project-setup)
  * [Running the project](#running-the-project)

  # Contributing
Most new code (unless it is fixes or patches to impossible java spaghetti) is required to be in the Kotlin language.

<h4>More information on Kotlin can be found <a href="https://kotlinlang.org/">here.</a></h4>

* **Wiki Editors**: Did you know the main 2009scape project has a wiki? Well it's always in need of people to fill it out and stay on top of it. If you're an active player and have the will, there's so much you could be helping out with over at the wiki. <a href="http://play.2009scape.org/wiki">Click here to go to the wiki.</a>


* **JSON editors**: Did you know that the vast majority of our drop tables, stats, examines, item info, npc info, etc is all stored in a very easy-to-modify format called JSON? This is something almost anyone can help with, especially with the tool made by Ceikry to make it simple and streamlined. If you want to know what can be done to help here, get in touch in the development channel in the discord! **JSON editors are always in need and always appreciated!!**.


## Content Developers: Setting Up the Project.
### GitLab Setup

<h4>Note: This allows you to commit changes to the main repo (with approval)! Also, always stay up to date with the most recent updates by pulling into your copy when redwings updates!</h4>

1. Create a GitLab account if you haven't done so already

3. Click "Fork" in the top right hand side of our  gitlab page.

**If at anytime you have an issue with gitlab please refer to the** <a href="https://gitlab.com/help">Gitlab help center.</a>


### Prerequisites

<h4>Note: It is required for a developer submitting a PR to use Intellij IDEA as your integrated development environment.</h4>

* Intellij IDEA
* Java SE Development Kit Version 11/OpenJDK 11 (preferred)
* Xampp

## Prereq Installation

<h3>Windows</h3>
<details>
<summary> </summary>

1. Install <a href="https://www.oracle.com/java/technologies/downloads/#java11-windows">JDK version 11</a>
    * Scroll down until you see Windows x86 and Windows x64
    * If you are running a 64bit verison of Windows (standard), select Windows x64
    * Accept the terms and conditions, after reading them of course, and login to oracle
    * Download and install like any normal application
    
2. Install <a href="https://www.apachefriends.org/index.html">Xampp</a>
    * Click on "Xampp for Windows"
    * Download
    * Install as Administrator
</details> 
    
<h3>MacOS</h3>
<details>
<summary> </summary>

1. Install <a href="https://www.oracle.com/java/technologies/downloads/#java11-mac">JDK version 11</a>
    * Scroll down until you see macOS x64
    * select macOS x64
    * Accept the terms and conditions, after reading them of course, and login to oracle
    * Download and install like any normal mac application
    
2. Install <a href="https://www.apachefriends.org/index.html">Xampp</a>
    * Click on "Xampp for OS X"
    * Download
    * Install as Administrator
</details>

<h3>Linux</h3>
<details>
<summary> </summary>

1. Install JDK version 11 through <a href="https://www.oracle.com/java/technologies/downloads/#java11-linux">Oracle</a> or through <a href="https://openjdk.java.net/install/">command line</a>
    * Debian, Ubuntu, etc.
    ```sh
    sudo apt install openjdk-11-jdk
    ```
    * Fedora, Oracle Linux, Red Hat Enterprise Linux, etc.
    ```sh
    su -c "yum install java-11-openjdk"
    ```
    
2. Install <a href="https://www.apachefriends.org/index.html">Xampp</a> OR Set up DB server of your choice.
    * Click install "XAMPP for Linux"
    * Install like any normal Linux program
</details>


### Project Setup

- Open Xampp (Skip if you roll your own database and know what you're doing.)
    * On Windows make sure you run Xampp as administrator
    * On the left-hand side make sure you tick the two "Service" boxes for Apache and MySQL
    * For both Apache and MySQL click "Start" under Actions
    * After doing that navigate to the <a href="https://localhost/phpmyadmin/">PHP My Admin LOCAL SITE</a>
    * Once opened, on the left-hand side click the three "disks" that say "New"
    * In the "Database name" bar type: global
    * Press the "Create" button
    * A three disk "global" should appear on the left hand side
    * Click it and on the top bar select "Import"
    * Under **FILE TO IMPORT** click "Browse...." 
    * Navigate to your 2009Scape project folder
    * Go to Server/db_exports/ and import global.sql
    * It may take a moment to import, when It is done importing Xampp is all set up!
    
### Running the project
***Note: If you choose not to use the provided run scripts, you *must* run `mvn clean` before it will build correctly.***

#### Linux / OSX
1. Make sure your database of choice is running (see above)
2. Start the game server with the included run script. E.G ./run g

#### Windows
1. Make sure your database is running (see above)
2. Start the Management Server with run-ms.bat
3. Start the game server with run-server.bat

### IDE Integration: IntelliJ IDEA (Recommended)
1. Click the "Project" tab on the left hand side.
2. In the top left where the "Project" drop down is, click Project Files
3. Expand the Management-Server folder
4. Right click pom.xml and click "Add Maven Project"
5. Expand the Server folder
6. Right click pom.xml and click "Add Maven Project"
7. In IntelliJ, File -> Invalidate Caches -> Invalidate Caches and Restart
8. You should now have ready-made run configurations in the top right: [Windows] Run MS, [Windows] Run Server, [Linux] Run MS, [Linux] Run Server
9. Go ahead and use the Run Server configuration for your OS to make sure everything worked.

### IDE Integration: Eclipse
Note: This section could use improvement. If you have a better way to integrate the project with eclipse, feel free to open an MR!
1. Make sure you have the [Eclipse Maven Plugin](https://stackoverflow.com/a/25993960/1971003) installed.
2. In Eclipse, click File -> Import
3. Type Maven in the search box
4. Under "Select an Import Source," select "Existing Maven Project."
5. Next
6. Click browse, and select the Management-Server folder.
7. Repeat steps 2-6 for the Server folder.

### IDE Integration: Netbeans
Note: This section could use improvement. If you have a better way to integrate the project with netbeans, feel free to open an MR!

1. Select File -> Open Project
2. Select the Management-Server folder to open the MS project.
3. Select the Server folder to open the Server project.
