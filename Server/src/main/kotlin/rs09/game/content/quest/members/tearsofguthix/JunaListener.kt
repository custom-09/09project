package rs09.game.content.quest.members.tearsofguthix

import api.animateScenery
import org.rs09.consts.Scenery
import rs09.game.interaction.InteractionListener

class JunaListener : InteractionListener {
    override fun defineListeners() {
        on(Scenery.JUNA_31302, SCENERY, "talk-to") { _, juna ->
            //animateScenery(juna.asScenery(), 2055) // lifts tail to let player pass
            //animateScenery(juna.asScenery(), 2056) // head moves down
            //animateScenery(juna.asScenery(), 2057) // lifts head up while talking

            //setVarbit(player, 449, 0, 450) //adds "tell story" to snek and changes her id to 31303

            animateScenery(juna.asScenery(), 2057)
            return@on true
        }
    }

}