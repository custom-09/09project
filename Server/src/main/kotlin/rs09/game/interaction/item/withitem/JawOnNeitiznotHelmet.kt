package rs09.game.interaction.item.withitem

import api.*
import core.tools.StringUtils
import org.rs09.consts.Items
import rs09.game.interaction.InteractionListener

class JawOnNeitiznotHelmet : InteractionListener {
    override fun defineListeners() {
        val JAW = 14749

        onUseWith(ITEM, JAW, Items.HELM_OF_NEITIZNOT_10828) {player, used, with ->
            if (removeItem(player, used.asItem(), Container.INVENTORY) && removeItem(player, with.asItem(), Container.INVENTORY)) {
                addItem(player, 14735)
                sendMessage(player, "You attach the basilisk jaw to the helm of Neitiznot.")
            }

            return@onUseWith true
        }
    }
}