package core.game.content.global;

import core.game.node.entity.player.Player;
import core.game.node.entity.skill.slayer.Tasks;
import core.game.node.item.Item;
import core.tools.RandomFunction;
import rs09.game.node.entity.skill.slayer.SlayerManager;
import rs09.game.world.repository.Repository;

/**
 * The BossKillcounter keeps track of the amount of bosses the player has slain.
 * addtoKillcount(player, npcId) should be added in the finalizeDeath() method of the combat handler for the boss.
 * @author Splinter
 */
public enum BossKillCounter {


	/* ORDINAL BOUND */
	KING_BLACK_DRAGON(new int[]{50}, "King Black Dragon", -1),
	BORK(new int[]{7133, 7134}, "Bork", -1),
	DAGANNOTH_SUPREME(new int[]{2881}, "Dagannoth Supreme", -1),
	DAGANNOTH_PRIME(new int[]{2882}, "Dagannoth Prime", -1),
	DAGANNOTH_REX(new int[]{2883}, "Dagannoth Rex", -1),
	CHAOS_ELEMENTAL(new int[]{3200}, "Chaos Elemental", -1),
	GIANT_MOLE(new int[]{3340}, "Giant Mole", -1),
	SARADOMIN(new int[]{6247}, "Commander Zilyana", 14737),
	ZAMORAK(new int[]{6203}, "K'ril Tsutsaroth", 14739),
	BANDOS(new int[]{6260}, "General Graardor", 14738),
	ARMADYL(new int[]{6222}, "Kree'arra", 14740),
	JAD(new int[]{2745}, "Tz-Tok Jad", 14736),
	KALPHITE_QUEEN(new int[]{1160}, "Kalphite Queen", -1),
	CORPOREAL_BEAST(new int[]{8133}, "Corporeal Beast", -1),
	TORMENTED_DEMONS(new int[]{
			8349, 8350, 8351, 8352, 8353, 8354,
			8355, 8356, 8357, 8358, 8359, 8360,
			8361, 8362, 8363, 8364, 8365, 8366,
	}, "Tormented demon", -1),
	//CALLISTO(new int[] { 8610 }, "Callisto", -1),
	//SCORPIA(new int[] { 8611 }, "Scorpia", -1),
	VENENATIS(new int[]{8591}, "Venenatis", -1),
	//VETION(new int[] { 8613 }, "Vet'ion", -1),
	//KRAKEN(new int[] { 8614 }, "Cave Kraken", -1),
	CRAZY_ARCHAEOLOGIST(new int[]{8592}, "Crazy Archaeologist", -1),


	;

	/**
	 * The npcs that can increase the killcounter
	 */
	private final int[] npc;

	/**
	 * The name of the NPC, to be displayed as a sendMessage
	 */
	private final String name;

	/**
	 * The item ID of the pet relating to the boss.
	 */
	private final int petId;

	/**
	 * Constructs a new {@code BossKillCounter} {@code Object}.
	 *
	 * @param npc  the npc.
	 * @param name the npc's string name
	 */
	BossKillCounter(final int[] npc, final String name, final int petId) {
		this.npc = npc;
		this.name = name;
		this.petId = petId;
	}

	/**
	 * Gets the npc.
	 *
	 * @return The npc.
	 */
	public int[] getNpc() {
		return npc;
	}

	/**
	 * Gets the NPC's name
	 *
	 * @return their name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the petId
	 *
	 * @return The petId
	 */
	public int getPetId() {
		return petId;
	}

	/**
	 * Gets the type for the npc.
	 *
	 * @param npc the npc.
	 * @return the BossKillcounter
	 */
	public static BossKillCounter forNPC(final int npc) {
		for (BossKillCounter kc : BossKillCounter.values()) {
			for (int i : kc.getNpc()) {
				if (npc == i) {
					return kc;
				}
			}
		}
		return null;
	}

	/**
	 * Adds to the player's killcount for that particular boss.
	 *
	 * @param killer The player who killed the npc
	 * @param npcid  the ID of the npc that just died
	 */
	public static void addtoKillcount(Player killer, int npcid) {
		if (killer == null) {
			return;
		}
		BossKillCounter boss = BossKillCounter.forNPC(npcid);
		if (boss == null) {
			return;
		}
		killer.getSavedData().getGlobalData().getBossCounters()[boss.ordinal()]++;
		killer.getPacketDispatch().sendMessage("Your " + boss.getName() + " killcount is now: <col=ff0000>" + killer.getSavedData().getGlobalData().getBossCounters()[boss.ordinal()] + "</col>.");
		addBossPet(killer, npcid, boss);
	}

	//	/**
//	 * Gives the player the pet if they killed a certain boss.
//	 * The chance by default is 1/5000.
//	 * Note: Not all bosses have pet versions of themselves.
//	 */
	private static void addBossPet(Player killer, int npcid, BossKillCounter boss) {
		if (boss.getPetId() == -1) { //The boss does not have a pet version.
			return;
		}
		int rand = RandomFunction.getRandom(5000);
		//if (npcid == 2745) { // If defeated Jad, make odds ~1/200
			//rand = RandomFunction.getRandom(200);
			//if (SlayerManager.getInstance(killer).getTask() == Tasks.JAD) { // If on Jad Slayer Task, make odds ~1/100
			//	rand = RandomFunction.getRandom(100);
			//}
		//} else if (npcid == 3200) { // If Chaos Elemental, make odds ~1/300
			//rand = RandomFunction.getRandom(300);
		//}
		//int rand = RandomFunction.getRandom(5000);
		if (rand == 1) {
			//for (int i = 0; i < killer.getFamiliarManager().hasPet()(new Item(boss.getPetId()) {
			//if (killer.getFamiliarManager().hasPet().get(i).getBabyItemId() == boss.getPetId()) {
			//return;
			//}
		//}
		if (killer.getFamiliarManager().hasFamiliar() && killer.getInventory().freeSlots() < 1) {
			return;
		}
		if (!killer.getFamiliarManager().hasFamiliar()) {
			killer.getFamiliarManager().summon(new Item(boss.getPetId()), true);
			killer.sendNotificationMessage("You have a funny feeling like you're being followed.");
		} else if (killer.getInventory().freeSlots() > 0) {
			killer.getInventory().add(new Item(boss.getPetId(), 1));
			killer.sendNotificationMessage("You feel something weird sneaking into your backpack.");
		}
		Repository.sendNews(killer.getUsername() + " has found a miniature " + (boss.equals(CORPOREAL_BEAST) ? "Dark core" : boss.getName()) + "!");
	}
}

	/**
	 * Increments the player's Barrows chest counter.
	 * @param player the player
	 */
	public static void addtoBarrowsCount(Player player) {
		if (player == null) {
			return;
		}
		player.getSavedData().getGlobalData().setBarrowsLoots(player.getSavedData().getGlobalData().getBarrowsLoots() + 1);
		player.getPacketDispatch().sendMessage("Your Barrows chest count is: <col=ff0000>" + player.getSavedData().getGlobalData().getBarrowsLoots() + "</col>.");
	}

}
