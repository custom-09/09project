package rs09.game.content.dialogue.region.tearsofguthixcavern

import api.setAttribute
import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import org.rs09.consts.NPCs
import rs09.game.content.dialogue.DialogueFile
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

class Juna_ToG_What_Are_The_Tears_DialogueFile() : DialogueFile() {

    val n = FacialExpression.NEUTRAL

    override fun handle(componentID: Int, buttonID: Int) {
        npc = NPC(NPCs.JUNA_2023)

        when (stage) {
            0 -> npcl(n, "The Third Age of the world was a time of great conflict, of " +
                    "destruction never seen before or since, when all the gods save Guthix " +
                    "warred for control.").also { stage++ }

            1 -> npcl(n, "The colossal wyrms, of whom today's dragons are a pale reflection, " +
                    "turned all the sky to fire, while on the ground armies of foot soldiers - " +
                    "goblins and trolls and humans - filled the valleys and plains with blood.").also { stage++ }

            2 -> npcl(n, "In the time the noise of the conflict woke Guthix from His deep slumber, " +
                    "and He rose and stood in the centre of the battlefield so the splendour of His " +
                    "wrath filled the world, and He called for the conflict to cease!").also { stage++ }

            3 -> npcl(n, "Silence fell, for the gods knew that none could challenge the power of the " +
                    "mighty Guthix; for His power is that of nature itself, to which all other things are " +
                    "subject, in the end.").also { stage++ }

            4 -> npcl(n, "Guthix reclaimed that which had been stolen from Him, and went back underground " +
                    "to His sleep and continue to draw the world's power into Himself.")

            5 -> npcl(n, "But on His way into the depths of the earth He sat and rested in this cave; and, " +
                    "thinking of the battle-scarred desert that now streched from one side of His world to the other, " +
                    "He wept.")

            6 -> npcl(n, "And so great was His sorrow, and so great was His life-giving power, that the " +
                    "rocks themselves began to weep with Him.")

            7 -> npcl(n, "Later, Guthix noticed that the rocks continued to weep, and that their tears " +
                    "was infused with a small part of His power.").also { stage++ }

            8 -> {
                npcl(n, "So He set me, His servant, to guard the cave, and He entrusted me the task " +
                        "of judging who was and was not worthy to access the tears.")
                setAttribute(player!!, "juna:explained-what-tog-are", true)
                stage = END_DIALOGUE
            }

        }
    }
}