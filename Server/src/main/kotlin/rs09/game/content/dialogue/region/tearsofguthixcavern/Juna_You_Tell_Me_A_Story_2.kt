package rs09.game.content.dialogue.region.tearsofguthixcavern

import core.game.content.dialogue.FacialExpression
import core.game.node.entity.npc.NPC
import org.rs09.consts.NPCs
import rs09.game.content.dialogue.DialogueFile
import rs09.tools.END_DIALOGUE

/**
 * @author qmqz
 */

class Juna_You_Tell_Me_A_Story_2() : DialogueFile() {

    val n = FacialExpression.NEUTRAL

    override fun handle(componentID: Int, buttonID: Int) {
        npc = NPC(NPCs.JUNA_2023)

        when (stage) {
            0 -> npcl(n, "Not after the start of my vigil a party of goblins happened " +
                    "on my cave. They were the ugly brutes of the gods' armies, but their " +
                    "armour bore a faded patch where the symbol of a god had been removed.").also { stage++ }

            1 -> npcl(n, "They looked around hesitantly, squinting in the light of crude " +
                    "torches, but as soon as they saw me they raised their spears and charged.").also { stage++ }

            2 -> npcl(n, "No one may access the Tears of Guthix by force. I sent them " +
                    "tumbling into the chasm.").also { stage++ }

            3 -> npcl(n, "Two hundred years later, another goblin found me. This one was" +
                    " unarmed, and although she was very wary I succeeded in engaging her in" +
                    " conversation.").also { stage++ }

            4 -> npcl(n, "Since then I have had many more visits from the descendants of " +
                    "those lost warriors, and over the centuries I have seen them change.").also { stage++ }

            5 -> npcl(n, "They have become timid rather than aggressive, and I have seen " +
                    "the light of intelligence grow in their bulging eyes.").also { stage++ }

            6 -> npcl(n, "Their stories are repetitive and grim, of scraping a living out " +
                    "of the harsh rock through ingenuity and toil shaping it into a home.").also { stage++ }

            7 -> npcl(n, "I have followed the progress of their race, but their individual " +
                    "stories hold little interest for me.").also { stage = END_DIALOGUE }

        }
    }
}