package rs09.game.content.quest.members.deathplateau

import api.*
import core.game.content.dialogue.FacialExpression
import org.rs09.consts.Items
import rs09.game.content.dialogue.DialogueFile

/**
 * @author qmqz
 */

class IOU_Note_Dialogue_File() : DialogueFile() {
    var a = 0
    override fun handle(interfaceId: Int, buttonId: Int) {

        when(stage) {
            0 -> player(FacialExpression.NEUTRAL, "The IOU says that Harold owes me some money.").also { stage++ }
            1 -> player(FacialExpression.EXTREMELY_SHOCKED, "Wait just a minute!").also { stage++ }
            2 -> playerl(FacialExpression.EXTREMELY_SHOCKED, "The IOU is written on the back of the combination!" +
                    "The stupid guard had it in his back pocket all the time!").also { stage++ }
            3 -> {
                removeItem(player!!, Items.IOU_3103)
                addItem(player!!, Items.COMBINATION_3102)
                sendItemDialogue(player!!, Items.COMBINATION_3102, "You have found the combination!").also { stage++ }
            }
            4 -> {
                openInterface(player!!, 220)
                setInterfaceText(player!!, "<col=3D1E00>Red is North of Blue. Yellow is South of Purple.", 220, 7)
                setInterfaceText(player!!, "<col=3D1E00>Green is North of Purple. Blue is West of", 220, 8)
                setInterfaceText(player!!, "<col=3D1E00>Yellow. Purple is East of Red.", 220, 9)
            }
        }
    }
}